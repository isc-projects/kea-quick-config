<?php

/*
  This takes arguments of min and max
  and type (integer, float) and a string to be checked
  This string is first checked if is_numeric
  Then it is settype to the appropriate type
  Then it is checked if it is between min / max
  as appropriate
  Return true (pass) false (fail)
*/
function ValidateNumericContent($type,$string,$min, $max) {
  if ($type != 'integer' && $type != 'float') {
    exit('type submitted to ValidateNumericContent() must be integer or float');
  }
  $return = true;
  if (!is_numeric($string)) {
    // non numeric string submitted
    $return = false;
  } else {
    settype($string,$type);
    if (isset($min) &&$min !== false && $string < $min) {
      // $string is less than $min and $min is set
      $return = false;
    }
    if (isset($max) && $max !== false && $string > $max) {
      // $string is greater than $max and $max is set
      $return = false;
    }
  }
  return($return);
}
