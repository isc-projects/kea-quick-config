<?php

// This include file is meant to send any header items that are needed as well as the <head></head> section of the HTML
function open() {
  // $output is the variable that will be filled with things and printed at the end
  global $output;

  $output.="<html>\n";
  $output.="<head>\n";
  $output.="<title>Kea Configuration Maker</title>\n";
  $output.="<script src=\"javascript/WriteItem.js\"></script>\n";
  $output.="<script src=\"javascript/ReadItem.js\"></script>\n";
  $output.="<script src=\"javascript/DelItem.js\"></script>\n";
  $output.="<script src=\"javascript/ShowDiv.js\"></script>\n";
  $output.="<script src=\"javascript/ProcessVals.js\"></script>\n";
  $output.="<script src=\"javascript/ProcessVals2.js\"></script>\n";
  $output.="<script src=\"javascript/AddAllFormData.js\"></script>\n";
  $output.="<script src=\"javascript/GetStoredFormData.js\"></script>\n";
  $output.="<script src=\"javascript/ClearAllVals.js\"></script>\n";
  $output.="<script src=\"javascript/ServerValidate.js\"></script>\n";
  $output.="<script src=\"javascript/PopulateAllSharedNetworksSelectBoxes.js\"></script>\n";
  $output.="<script src=\"javascript/populateSharedNetworks.js\"></script>\n";
  $output.="<script src=\"javascript/copy2clipboard.js\"></script>\n";
  $output.="<script src=\"javascript/serverGetConfig.js\"></script>\n";
  $output.="<script src=\"javascript/ServerComm.js\"></script>\n";
  $output.="<script src=\"javascript/toggleButton.js\"></script>\n";
  $output.="<script src=\"javascript/resettheEnd.js\"></script>\n";
  $output.="<script src=\"javascript/showRelnotes.js\"></script>\n";
  $output.="<script src=\"javascript/moreSharedNetworks.js\"></script>\n";
  $output.="<script src=\"javascript/morePools.js\"></script>\n";
  $output.="<script src=\"javascript/moreSubnets.js\"></script>\n";
  $output.="<script src=\"javascript/globalSettingsList.js\"></script>\n";
  $output.="<script src=\"javascript/GlobalSettingsSelectBoxPopulate.js\"></script>\n";
  $output.="<script src=\"javascript/moreGlobalSettings.js\"></script>\n";
  $output.="<script src=\"javascript/globalOptionsList.js\"></script>\n";
  $output.="<script src=\"javascript/GlobalOptionsSelectBoxPopulate.js\"></script>\n";
  $output.="<script src=\"javascript/moreGlobalOptions.js\"></script>\n";
  $output.="<script src=\"javascript/moreClientClasses.js\"></script>\n";
  $output.="<script src=\"javascript/moreReservations.js\"></script>\n";
  $output.="<script src=\"javascript/GetConfiguredClasses.js\"></script>\n";
  $output.="<script src=\"javascript/updateClassSelects.js\"></script>\n";
  $output.="<script src=\"javascript/ValidateConfType.js\"></script>\n";
  $output.="<script src=\"javascript/StoreValidatedData.js\"></script>\n";
  $output.="<link rel=\"stylesheet\" href=\"style.css\">\n";
  $output.="<link rel=\"icon\" type=\"image/png\" href=\"images/kea-imageonly-64x64.png\">\n";
  $output.="</head>\n";
  $output.="<body onload=\"ShowDiv('ConfTypeChooser');moreSharedNetworks(1);moreSubnets(1);morePools(1,1);moreClientClasses(1);moreReservations(1);GetStoredFormData();\">\n";
  $output.="<div class=MainView>\n";
  // Thats it for now!

}
