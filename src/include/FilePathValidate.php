<?php

// validate submitted file path
// can't really check much as we don't have access to the filesystem
// check that it starts with '/' character.
// check that it doesn't end with a '/' character.
// default parameter $ignoreTrailslash controls whether checking 
// for a trailing / is performed.  If it is false, the check 
// is performed
// check that it does not contain a '"' chracter

function FilePathValidate($path,$ignoreTrailslash = false) {
  // default
  $result='PASS';
  $error='NULL';
  // is there anything other than a '/' at the beginning?
  if (!preg_match('/^\//',$path)) {
    $result='FAIL';
    $error='File location must begin with a /';
  }
  // it should not end with a '/'
  if (preg_match('/\/$/',$path) && !$ignoreTrailslash) {
    $result='FAIL';
    $error='File location must not end with a /';
  }
  // It should not contain " as that will break the json
  if (preg_match('/\"/',$path)) {
    $result='FAIL';
    $error='File location must not contain a "';
  }
  return(array($result,$error));
}
