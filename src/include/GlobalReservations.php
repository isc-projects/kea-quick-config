<?php

// This uses data found in $_POST that is related to one or more global host reservations
// that were configured during the 'Define Host Reservations' step.  This data is then added
// to the incoming $arr array in the same way that is done in other functions.

function GlobalReservations($arr) {

  // Will contain the discovered values inside a main array that will be set and returned in $arr
  $main=array();

  // foreach $_POST we will look for reservation-hwaddr substring and then extract ID from the key
  // then grab the names and values of that and any other parameters that are to be added.
  // add these to the $main array.  Should be fairly simple.  No need of validation as it already happened.
  foreach ($_POST as $key => $value) {
    if (substr($key,0,18) == 'reservation-hwaddr') {
      // init record array
      $record=array();
      if (!empty($value)) {
        $record['hw-address']=$value;
      }
      // get the id
      $n=substr($key,18);
      if (!empty($_POST['reservation-hostname'.$n])) {
        $record['hostname']=$_POST['reservation-hostname'.$n];
      }
      // Add configured options for the reservation
      $prefix='Reservation'.$n.'_|NGO|_';
      $x=array();
      $x=GlobalOptions($x,$prefix);
      $record = $record + $x;
      if (isset($_POST['reservation-client-classes'.$n])) {
        // add class name content to client-classes array
        // they arrive in this format: SomeClass1_|_SomeClass2 and so need split
        $classes=explode('_|_',$_POST['reservation-client-classes'.$n]);
        foreach ($classes as $junk => $class) {
          // iterate resultant array adding each $class to the array
          if (!empty($class)) {
            $record['client-classes'][]=$class;
          }
        }
      }
      if (!empty($record)) {
        $main[]=$record;
      }
    }
  }

  // add the created array here
  if (!empty($main)) {
    $arr['reservations']=$main;
  }
  // return the building array
  return($arr);
}
