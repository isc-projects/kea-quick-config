<?php

function CheckEmpties() {
  // check for empties among things where
  // it is possible that they could be empty
  if (empty($_POST['socket'])) {
    $_POST['socket'] = '';
  }
  if (empty($_POST['LDname'])) {
    $_POST['LDname']='';
  }
  if (empty($_POST['LDuserName'])) {
    $_POST['LDuserName']='';
  }
  if (empty($_POST['LDpassWord'])) {
    $_POST['LDpassWord']='';
  }
  if (empty($_POST['DNSserver1'])) {
    $_POST['DNSserver1'] = '';
  }
  if (empty($_POST['DNSserver2'])) {
    $_POST['DNSserver2'] = '';
  }
  if (empty($_POST['domainName'])) {
    $_POST['domainName'] = '';
  }
  if (empty($_POST['NTPserver1'])) {
    $_POST['NTPserver1'] = '';
  }
  if (empty($_POST['NTPserver2'])) {
    $_POST['NTPserver2'] = '';
  }
  if (empty($_POST['loggingDirectory'])) {
    $_POST['loggingDirectory']='';
  }
}
