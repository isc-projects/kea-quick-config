<?php

function Networks($arr,$confType) {
  // dhcp6 not yet supported
  if ($confType=='dhcp6') {
    error("DHCPv6 configurations not yet supported.");
  }

  // all data should have already been validated
  // one or more items configured below will be dependant upon the 
  // type of configuration chosen (i.e., dhcp4 or dhcp6)

  // sharedNetworksArray contains the list of subnets that are in shared networks
  $sharedNetworksArray=array();
  // subnetsArray contains a list of subnets that are NOT in shared networks
  $subnetsArray=array();

  // first, find all the subnets that were configured by looking
  // for all the $_POST['subnetn'] where 'n' is a positive integer
  // beginning at 1.
  $found=true;
  $n=1;
  $SID=1;
  while ($found) {
    if (!empty($_POST['subnet'.$n])) {
      // Gather other details following same 'n' rules:
      // $_POST['routern'] where 'n' is the known integer
      $subnet=$_POST['subnet'.$n];
      $router='';
      if (!empty($_POST['router'.$n])) {
        $router=$_POST['router'.$n];
      }
      $pools=array();
      // for $_POST['pooln-o'] where 'n' is the known integer and o is a
      // positive integer starting at 1
      $found2=true;
      $o=1;
      while ($found2) {
        if (!empty($_POST['pool'.$n.'-'.$o])) {
          $pools[$o]['pool']=$_POST['pool'.$n.'-'.$o];
          if (!empty($_POST['poolClientClass'.$n.'-'.$o])) {
            $pools[$o]['client-class']=$_POST['poolClientClass'.$n.'-'.$o];
          }
          $o++;
        } else {
          $found2=false;
        }
      }
      if (!empty($_POST['subnetSharedNetworkSelect'.$n])) {
        // is it a member of a shared network? add to shared network array
        $sharedNetworksArray[$_POST['subnetSharedNetworkSelect'.$n]][]=array(
          'subnet' => $subnet,
          'id' => $SID,
          'router' => $router,
          'pools' => $pools
        );
        $SID++;
      } else {
        // is it not a member of shared nework?  add to subnets array
        $subnetsArray[$n]=array(
          'subnet' => $subnet,
          'id' => $SID,
          'router' => $router,
          'pools' => $pools
        );
        $SID++;
      }
      $n++;
    } else {
      $found=false;
    }
  }
  // execute shared network build function
  if (!empty($sharedNetworksArray)) {
    $arr=SharedNetworksBuild($arr,$sharedNetworksArray,$confType);
  }
  // execute subnet build function
  if (!empty($subnetsArray)) {
    $arr=SubnetsBuild($arr,$subnetsArray,$confType);
  }

  // return the array now
  return($arr);
}
