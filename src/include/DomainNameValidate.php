<?php

// this will validate domain names 
// though all it will do is check for empty labels
// otherwise, I don't think we can predict what
// might be typed as it could be international

function DomainNameValidate($domain) {
  $return=true;
  $error='';
  // split on a .
  $labels=explode('.',$domain);
  // look for empty labels
  foreach ($labels as $key => $label) {
    if (empty($label)) {
      $return=false;
      $error="empty label found in domain: ".$domain;
    }
    // make sure none of the labels contain whitespace:
    if (preg_match('/\s/',$label)) {
      $return=false;
      $error="whitespace found in domain: ".$domain;
    }
    // make sure none of the labels contain a `"` character:
    if (preg_match('/\"/',$label)) {
      $return=false;
      $error="found illegal character (\") in domain: ".$domain;
    }
  }
  // make sure it isn't an IP:
  if (validIP($domain)) {
    $return=false;
    $error=$domain." is an IP address where domain expected";
  }
  return(array($return,$error));
}
