<?php

// This uses data found in $_POST that is related to one or more client classes
// that were configured during the 'Define Client Classes' step.  This data is
// then added to the incoming $arr array in the same way that is done in other
// functions.

function ClientClasses($arr) {
  // sort the $_POST first so that the keys are in the intended order
  ksort($_POST,SORT_NATURAL);
  // main array
  $main=array();
  // step one, perform foreach on $_POST and look for keys that begin with clientClass-name
  foreach ($_POST as $key => $value) {
    if (substr($key,0,16) == 'clientClass-name') {
      // step two, obtain the index number from the full key with substr
      $n=substr($key,16);
      // step three, confirm that clientClass-name.$n has a value
      if (isset($_POST['clientClass-name'.$n]) && !empty($_POST['clientClass-name'.$n])) {
        // step four, confirm that there is a corresponding clientClass-tsel.$n that has a value
        if (isset($_POST['clientClass-tsel'.$n])) {
          // step five, confirm that there is a corresponding clientClass-tinp.$n that has a value
          if (isset($_POST['clientClass-tinp'.$n])) {
            // step six, add this to array and add that array to main array that will be added to
            // Create record array
            $record=array();
            // Add 'name' to record array
            $record['name']=$_POST['clientClass-name'.$n];
            if (!empty($_POST['clientClass-tsel'.$n]) && !empty($_POST['clientClass-tinp'.$n])) {
              $s=$_POST['clientClass-tsel'.$n];
              $v=$_POST['clientClass-tinp'.$n];
              // The test line is a little more complex as we need special rules for the different test functions
              if ($s=="hexstring(pkt4.mac, ':')") {
                $test="$s == '$v'";
              } else if ($s=="option[82].option[1].hex") {
                $test="$s == '$v'";
              } else if ($s=="option[82].option[2].hex") {
                $test="$s == '$v'";
              } else if ($s=="member()") {
                $test="member('$v')";
              } else if ($s=="not member()") {
                $test="not member('$v')";
              } else {
                die("Unknown test expression encountered (".$s.") while evaluating client-classes");
              }
              // add 'test' to record (but not ones that contain "member()" as those must be added last.
              $record['test']=$test;
            }
            // add options to record
            $prefix='ClientClass'.$n.'_|NGO|_';
            $x=array();
            $x=GlobalOptions($x,$prefix);
            $record = $record + $x;
            // Add record to main
            if (!settype($n,'integer')) {
              die("Failed to settype(".$n.",'integer') while evaluating client-classes");
            }
            $main[]=$record;
          }
        }
      }
    }
  }

  // add main array to $arr as client-class
  if (!empty($main)) {
    $arr['client-classes']=$main;
  }
  return($arr);
}
