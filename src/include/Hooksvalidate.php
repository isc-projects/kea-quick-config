<?php

// validate HooksSetupForm content
// So far, there is only the HA
// hook content to be validated
// this is the IP addresses for
// server1 and server2  These
// can be IPv4 or IPv6

function Hooksvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';
  // these fields should at least exist
  if (empty($_POST['HooksHA'])) {
    $_POST['HooksHA']='';
  }
  if (empty($_POST['HooksServer1ip'])) {
    $_POST['HooksServer1ip']='';
  }
  if (empty($_POST['HooksServer2ip'])) {
    $_POST['HooksServer2ip']='';
  }
  // Only if HooksHA is checked
  if ($_POST['HooksHA']=='libdhcp_ha.so') {
    if (validIP($_POST['HooksServer1ip']) && $_POST['HooksServer1ip']) {
      // valid IP - change nothing
    } else {
      // bad IP - alter returns
      $result='FAIL';
      $field='HooksServer1ip';
      $error='A valid IPv4 or IPv6 address is  required';
    }
    if (validIP($_POST['HooksServer2ip']) && $_POST['HooksServer2ip']) {
      // valid IP - change nothing
    } else {
      // bad IP - alter returns
      $result='FAIL';
      $field='HooksServer2ip';
      $error='A valid IPv4 or IPv6 address is  required';
    }
    if ($_POST['HooksServer1ip'] == $_POST['HooksServer2ip']) {
      // The fields cannot contain the same address
      $result='FAIL';
      $field='HooksServer2ip';
      $error='The fields cannot contain the same address';
    }
  }
  return(array($result,$field,$error));
}

