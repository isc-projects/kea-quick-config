<?php

// Function to confirm ip is a member of valid IPv6 subnet
function InSubnet($ip,$CIDR) {
  // the incoming ip and CIDR need to have been validated as valid ip and valid subnet 
  // prior to this function so that this function can work for ipv4 or ipv6 as the same
  // method will work for both, i believe.

  // split the net and mask on '/'
  list($net,$mask) = explode('/',$CIDR);

  // convert ip to bits
  $ipBits=bits($ip);

  // convert net to bits
  $netBits=bits($net);

  // substring ip 0,mask
  $ipBitsSubstr=substr($ipBits,0,$mask);

  // substring net 0,mask
  $netBitsSubstr=substr($netBits,0,$mask);

  // compare resultant substrings - if they are = then ip is in subnet
  if ($ipBitsSubstr == $netBitsSubstr) {
    return(true);
  } else {
    return(false);
  }
}

