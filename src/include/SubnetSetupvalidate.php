<?php

// validate SubnetSetupForm content
// This is the meat of the validation really.
// There will be one or more subnets specified here.
// A foreach loop to detect the subnets and validate
// will be needed.  Will need to detect what config
// type was selected and act accordingly
// though currently, DHCPv6 is not supported.

function SubnetSetupvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // What type are we supposed to validate?
  // detect if DHCPv6 type was chosen and show an error
  // as it is currently not supported
  if (empty($_POST['ConfType'])) {
    $result='FAIL';
    $field='ARMLINK';
    $error='Config type missing';
  } else {
    if ($_POST['ConfType']=='dhcp4') {
      $type='IPv4';
    } else if ($_POST['ConfType']=='dhcp6') {
      $type='IPv6';
      $result='FAIL';
      $field='ARMLINK';
      $error='DHCPv6 is not currently supported';
    } else {
      // unknown type!
      $result='FAIL';
      $field='ARMLINK';
      $error='Invalid config type chosen: '.$_POST['ConfType'];
    }
  }

  // Here lets validate the options that are set in the subnets.  Need to tease out the prefix and then execute the function
  $prefixChecked=array();
  foreach ($_POST as $key => $value) {
    if (preg_match('/\_\|NGO\|\_/',$key)) {
      list($prefix,$junk)=explode('_|NGO|_',$key);
      if (empty($prefixChecked[$prefix])) {
        list($a,$b,$c)=nonGlobalOptionsvalidate($prefix);
        if ($a=='FAIL') {
          $result=$a;
          $field=$b;
          $error=$c;
        }
        $prefixChecked[$prefix]=true;
      }
    }
  }

  // foreach _POST look for /^subnet/
  // Then validate the rest that goes with it
  // A similar loop will be needed inside for pools
  foreach ($_POST as $key => $value) {
    if (preg_match('/^subnet/',$key)) {
      // we found a subnet, lets get the n part
      $n=substr($key,6);
      if ($n && is_numeric($n)) {
        settype($n,"integer");
        // validate subnet
        if (!ValidSubnet($value)) {
          $result='FAIL';
          $field=$key;
          $error=$value.' is not a valid subnet';
        }
        // confirm that it is correct type
        $test=is4or6($value);
        if ($type==$test) {
        } else {
          $result='FAIL';
          $field=$key;
          $error=$value.' is not valid when configuring '.$_POST['ConfType'];
        } 
        // router is optional
        $router='';
        if (!empty($_POST['router'.$n])) {
          $router=$_POST['router'.$n];
          // validate router (only thing we can look at is ... is the router in the subnet?)
          if (InSubnet($router,$value)) {
            // Great! The router is a part of the configured subnet
          } else {
            // Router ip specified outside of subnet
            $result='FAIL';
            $field='router'.$n;
            $error=$router.' is not contained within subnet '.$value;
          }
        }
        // find pools and validate (deserves its own function I think)
        // pool(s) are also optional
        list($x,$y,$z)=ValidatePools($router,$value,$n,$type);
        if ($x=='FAIL') {
          $result=$x;
          $field=$y;
          $error=$z;
        }
      }
    }
  }
  //$stdout = fopen('php://stdout', 'w');
  //fwrite($stdout, "($result,$field,$error)\n");
  return(array($result,$field,$error));
}
