<?php

function ValidSubnet($CIDR) {
  // make sure it has a '/'
  if (str_contains($CIDR,'/')) {
  } else {
    return(false);
  }
  // split the parts
  list($net,$mask) = explode('/',$CIDR);
  if (!validIP($net)) {
    // the net part isn't a valid ipv4
    return(false);
  }
  if (!is_numeric($mask)) {
    // the mask is not numeric
    return(false);
  }
  settype($mask,"integer");
  // if $net is IPv4 then mask should be 0-32 (for /0 or /32)
  // if $net is IPv6 then mask should be 0-128 (for /0 or /128)
  $max=256;
  if (filter_var($net,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV4))) {
    $max=32;
  } else if (filter_var($net,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV6))) {
    $max=128;
  }
  if ($mask < 0 || $mask > $max) {
    // mask was invalid
    return(false);
  }
  // get a binary representation
  $bin=bits($net);
  // get the masked off bits
  $chopped=substr($bin,$mask);
  // Check the "chopped" portion.  If there is anything but 0 in there, its not a valid subnet
  if (str_contains($chopped,'1')) {
    // there was at least one '1' in the resultant string.  Therefore, the subnet is not valid
    return(false);
  } else if (str_contains($chopped,'0')) {
    return(true);
  } else {
    // this shouldn't happen
    return(false);
  }
}
