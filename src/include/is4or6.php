<?php

// filter incoming IP to find out if it is an ipv4 or ipv6 address
function is4or6($ip) {
  // ip may contain a /mask so we need to strip that if so
  if (str_contains($ip,'/')) {
    list($ip,$junk)=explode('/',$ip);
  }
  // filter ip on ipv4 to see if it is an ipv4 address
  $ip4=filter_var($ip,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV4));
  // filter ip on ipv6 to see if it is an ipv6 address
  $ip6=filter_var($ip,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV6));
  // return type or false if no type found
  if ($ip4) {
    return('IPv4');
  } else if ($ip6) {
    return('IPv6');
  } else {
    return(false);
  }
}
