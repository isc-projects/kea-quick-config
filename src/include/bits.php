<?php

// get bits that make up an ipv6 address (all 128 of them) represented as a string of 0s and 1s
function bits($inet) {
   // convert to address from string
   $inet=inet_pton($inet);
   // split to an array of char
   $splitted=str_split($inet);
   // init return string
   $binaryip='';
   foreach ($splitted as $char) {
     // pad to 128 bits
     $binaryip.=str_pad(decbin(ord($char)),8,'0',STR_PAD_LEFT);
   }
   // return
   return($binaryip);
}
