<?php

// validate Client Classification Setup Form content
// There are three things to validate in $_POST:
// clientClass-name: will consult ARM to see what is valid here
// clientClass-tsel: This contains a static list which informs validation of
// clientClass-tinp: which is validated based on what was chosen in the above tsel

function ClientClassificationSetupvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // save array of 'clientClass-name'.$n and members for use in validating the member() / not member() evaluation types
  $ClassNames=array('ALL','KNOWN','UNKNOWN');
  // first check if there are any classes defined that do not have all three of the required fields completed
  foreach ($_POST as $key => $value) {
    if (substr($key,0,16) == 'clientClass-name') {
      //$n=substr($key,16);
      //if (!isset($_POST['clientClass-tsel'.$n]) || !isset($_POST['clientClass-tinp'.$n])) {
      //  $result='FAIL';
      //  $field='clientClass-name'.$n;
      //  $error='Client Class, Test Expression and Test Value must all be completed';
      //}
    } else if (substr($key,0,16) == 'clientClass-tsel') {
      $n=substr($key,16);
      if (!isset($_POST['clientClass-name'.$n]) || !isset($_POST['clientClass-tinp'.$n])) {
        $result='FAIL';
        $field='clientClass-tsel'.$n;
        $error='Client Class and Test Value must be completed if a Test Expression is chosen';
      }
    } else if (substr($key,0,16) == 'clientClass-tinp') {
      $n=substr($key,16);
      if (!isset($_POST['clientClass-tsel'.$n]) || !isset($_POST['clientClass-name'.$n])) {
        $result='FAIL';
        $field='clientClass-tinp'.$n;
        $error='Client Class and Test Expression must be completed if Test Value is specified';
      }
    }
  }

  // Here lets validate the options that are set in the classes.  Need to tease out the prefix and then execute the function
  $prefixChecked=array();
  foreach ($_POST as $key => $value) {
    if (preg_match('/\_\|NGO\|\_/',$key)) {
      list($prefix,$junk)=explode('_|NGO|_',$key);
      if (empty($prefixChecked[$prefix])) {
        list($a,$b,$c)=nonGlobalOptionsvalidate($prefix);
        if ($a=='FAIL') {
          $result=$a;
          $field=$b;
          $error=$c;
        }
        $prefixChecked[$prefix]=true;
      }
    }
  }

  // will need to foreach the $_POST array as we do not know the exact form field names
  foreach ($_POST as $key => $value) {
    if (substr($key,0,16) == 'clientClass-name') {
      // Get the n portion
      $n=substr($key,16);
      if (isset($_POST['clientClass-name'.$n])) {
        // clientClass-name - this is apparently just a string and Kea doesn't care about spaces or special characters
        if ($_POST['clientClass-name'.$n] == '') {
          // Nothing to validate here as this is string content other than it needs to have content
          // It shouldn't be possible for the name to be empty and the rest of the fields be considered
          $result='FAIL';
          $field='clientClass-name'.$n;
          $error='Client Class name must contain a value';
        }
        // save the classname value
        $ClassNames[]=$_POST['clientClass-name'.$n];
        // clientClass-tsel
        if (isset($_POST['clientClass-tsel'.$n])) {
          $Evaluation=$_POST['clientClass-tsel'.$n];
          // This should be one of the pre-configured possible values
          // clientClass-tinp is validated as a consequence of what was chosen in clientClass-tsel
          if (empty($_POST['clientClass-tinp'.$n])) {
            $_POST['clientClass-tinp'.$n]='';
          }
          $EvalValue=$_POST['clientClass-tinp'.$n];
          if ($Evaluation == '') {
            // This can be undefined
          } else if ($Evaluation == "hexstring(pkt4.mac, ':')") {
            // Values here should be of the format: 00:00:00:00:00:00
            if (!validhwaddr($EvalValue)) {
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error=$EvalValue.' must be a valid MAC address when choosing '.$Evaluation;
            }
          } else if ($Evaluation == "option[82].option[1].hex") {
            // This value is an opaque string ... no validation possible other than it must not be blank
            if (!$EvalValue) {
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error='Test Value cannot be blank when '.$Evaluation.' is chosen';
            }
          } else if ($Evaluation == "option[82].option[2].hex") {
            // This value is an opaque string ... no validation possible other than it must not be blank
            if (!$EvalValue) {
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error='Test Value cannot be blank when '.$Evaluation.' is chosen';
            }
          } else if ($Evaluation == "member()") {
            if (!$EvalValue) {
              // This value should not be empty if the expression is set
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error='Test Value cannot be blank when '.$Evaluation.' is chosen';
            } else if (!in_array($EvalValue, $ClassNames)) {
              // The value of member() needs to exist by now as Kea will fail to load if a non-existant (yet) class is specified
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error=$EvalValue.' is not a specified Client Class, the class must exist prior to specification in the member() / not member() expression.';
            }
          } else if ($Evaluation == "not member()") {
            // This value should not be empty if the expression is set
            if (!$EvalValue) {
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error='Test Value cannot be blank when '.$Evaluation.' is chosen';
            } else if (!in_array($EvalValue, $ClassNames)) {
              // The value of member() needs to exist by now as Kea will fail to load if a non-existant (yet) class is specified
              $result='FAIL';
              $field='clientClass-tinp'.$n;
              $error=$EvalValue.' is not a specified Client Class, the class must exist prior to specification in the member() / not member() expression.';
            }
          } else {
            $result='FAIL';
            $field='clientClass-tsel'.$n;
            $error='Unknown selection in field';
          }
        }
      }
    }
  }
  return(array($result,$field,$error));
}

/*
example client-classes

"client-classes": [
  {
    "name": "some test of names !@#$%^&*()_+",
    "test": "not member('KNOWN')"
  },
  {
    "name": "NoTestLine"
  },
  {
    "name": "Test_pkt4.mac",
    "test": "hexstring(pkt4.mac, ':') == '00:0c:01:02:03:04'"
  },
  {
    "name": "Test_option82.1",
    "test": "option[82].option[1].hex == 'vlan412'"
    // 76:6c:61:6e:34:31:32
    // vlan412
  },
  {
    "name": "Test_option82.2",
    "test": "option[82].option[2].hex == 'SOME ASCII STRING'"
    // 53:4f:4d:45:20:41:53:43:49:49:20:53:54:52:49:4e:47
    // SOME ASCII STRING
  },
  {
    "name": "Test_member()",
    "test": "member('Test_option82.1')"
  },
  {
    "name": "Test_not member()",
    "test": "not member('Test_option82.2')"
  }
],

*/
