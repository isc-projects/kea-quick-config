<?php
function CheckForInvalidCharacters() {

  // Set some sensible defaults
  $result='FAIL';
  $field='ARMLINK';
  $error='Unable to validate supplied content';

  $invalidC=false;
  // lets look through all _POST fields looking for the bad characters
  $bad=array('"','\'','\\');
  foreach ($_POST as $key => $value) {
    foreach ($bad as $junk => $c) {
      if (str_contains($value,$c)) {
        // bad character found
        // is it a "'" ?  If so, is the field clientClass-tsel?
        if ($c == '\'' && substr($key,0,16) == 'clientClass-tsel') {
          // then ignore as this field may need to contain a single quote
        } else {
          $result='FAIL';
          $field=$key;
          $error='Found invalid character '.$c;
          $invalidC=true;
        }
      }
    }
  }
  return(array($result,$field,$error,$invalidC));
}
