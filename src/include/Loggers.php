<?php

function Loggers($arr,$dir,$confType) {
  // this function sets up the loggers section of the config
  // currently its pretty boring just taking a dir and setting
  // up some default logging.  There are some future plans to
  // make it more interesting.  Not sure exactly yet...
 
  // again - the data should have already been validated

  // dhcp6 not yet supported
  if ($confType=='dhcp6') {
    error("DHCPv6 configurations not yet supported.");
  }

  if ($dir) {
    // strip trailing '/' character from $dir
    $dir=preg_replace('/\/$/','',$dir);
    // kea-dhcp4
    $output_options=array();
    $output_options[]=array(
      'output' => $dir.'/dhcp4.log',
      'maxver' => 10
    );
    $arr['loggers'][]=array(
      'name' => 'kea-dhcp4',
      'severity' => 'INFO',
      'output_options' => $output_options
    );

    // kea-dhcp4.dhcpsrv
    $output_options=array();
    $output_options[]=array(
      'output' => $dir.'/dhcp4-dhcpsrv.log',
      'maxver' => 10
    );
    $arr['loggers'][]=array(
      'name' => 'kea-dhcp4.dhcpsrv',
      'severity' => 'INFO',
      'output_options' => $output_options
    );

    // kea-dhcp4.leases
    $output_options=array();
    $output_options[]=array(
      'output' => $dir.'/dhcp4-leases.log',
      'maxver' => 10
    );
    $arr['loggers'][]=array(
      'name' => 'kea-dhcp4.leases',
      'severity' => 'INFO',
      'output_options' => $output_options
    );
  } else {
    // logging directory was not specified, ouput only 
    // a simple logging configuration to stdout
    $output_options=array();
    $output_options[]=array(
      'output' => 'stdout'
    );
    $arr['loggers'][]=array(
      'name' => 'kea-dhcp4',
      'severity' => 'INFO',
      'output_options' => $output_options
    );
  }

  // return $arr
  return($arr);
}
