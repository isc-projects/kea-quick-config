<?php

// validate ConfTypeChooserForm content
// This validation will be a little more exciting
// it is conditional depending on the type chosen

function LeaseDatabaseSetupvalidate() {
  // set some defaults
  $result='PASS';
  $field='NULL';
  $error='NULL';
  if (empty($_POST['LeaseDatabaseType'])) {
    $_POST['LeaseDatabaseType']='';
  }
  if (empty($_POST['LDname'])) {
    $_POST['LDname']='';
  }
  if (empty($_POST['LDuserName'])) {
    $_POST['LDuserName']='';
  }
  if (empty($_POST['LDpassWord'])) {
    $_POST['LDpassWord']='';
  }

  if ($_POST['LeaseDatabaseType']=='memfile') {
    // is it memfile?
    // Name should contain a valid file path
    if (!empty($_POST['LDname'])) {
      list($result,$error)=FilePathValidate($_POST['LDname']);
      if ($result == 'FAIL') {
        $field='LDname';
      }
    } else {
      // empty is allowed and will generate a non-persistant memfile (memory only) configuration
    }
    // Username should be blank
    if (!empty($_POST['LDuserName'])) {
      $result='FAIL';
      $field='LDuserName';
      $error='Username should be left blank when memfile is chosen';
    }
    // Password should be blank
    if (!empty($_POST['LDpassWord'])) {
      $result='FAIL';
      $field='LDpassWord';
      $error='Password should be left blank when memfile is chosen';
    }
  } else if ($_POST['LeaseDatabaseType']=='mysql' || $_POST['LeaseDatabaseType']=='postgresql') {
    // is it mysql or postgresql?
    // Name, Username, and Password are all required.  Limits on included characters are outside the scope.
    if (empty($_POST['LDname'])) {
      $result='FAIL';
      $field='LDname';
      $error='Name should not be blank when '.$_POST['LeaseDatabaseType'].' is chosen';
    }
    if (empty($_POST['LDuserName'])) {
      $result='FAIL';
      $field='LDuserName';
      $error='Username should not be blank when '.$_POST['LeaseDatabaseType'].' is chosen';
    }
    if (empty($_POST['LDpassWord'])) {
      $result='FAIL';
      $field='LDpassWord';
      $error='Password should not be blank when '.$_POST['LeaseDatabaseType'].' is chosen';
    }
  } else {
    // it is unknown or not selected at all
    $result='FAIL';
    $field='LeaseDatabaseType';
    $error='Type is a required field';
  }
  return(array($result,$field,$error));
}
