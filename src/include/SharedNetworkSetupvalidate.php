<?php

// validate SharedNetworkSetupForm content
// This is a list of one or more shared networks
// they can be any string but must be unique

function SharedNetworkSetupvalidate() {

  $result='PASS';
  $field='NULL';
  $error='NULL';

  $sharedNetworksFound=array();
  foreach ($_POST as $key => $value) {
    // there could be many sharedNetworkn
    // where 'n' is a number beginning at 1
    if (preg_match('/^sharedNetwork/',$key)) {
      if ($value) {
        // nothing really to test as it is a string of any kind
        // The ARM doesn't say, so I assume case insenstive
        $small=strtolower($value);
        if (in_array($small,$sharedNetworksFound)) {
          // just make sure it is unique
          $result='FAIL';
          $field=$key;
          $error=$value.' is a duplicate.  Each Shared Network name must be unique';
        }
        $sharedNetworksFound[]=$small;
      }
    }
  }
  return(array($result,$field,$error));
}
