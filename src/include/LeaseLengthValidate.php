<?php

function LeaseLengthValidate($secs) {
  $result=true;
  $error='';
  // $secs will be a string as it is $_POST content
  // test if it is numeric
  if (is_numeric($secs)) {
    // since it is a string, we will need to use a regex match
    // to confirm that it is only postive numbers
    if (preg_match('/^[0-9]+$/',$secs)) {
      // only 0-9 characters in the string
      // it is probably valid
      // technically, this test is the only one needed
      // but the is_numeric() test was used initially
    } else {
      // oops ... there is some invalid character
      $result=false;
      $error='Lease Length must be a positive integer';
    }
  } else {
    $result=false;
    $error='Lease Length must be a positive integer';
  }
  return(array($result,$error));
}
