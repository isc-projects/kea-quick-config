<?php

// validate GlobalSettingsForm content
// This is a list of one or more Global settings
// there will need to be a specific validation for
// most of them though there could be some overlap
// they are, for the most part, disparate settings.

function GlobalSettingsvalidate() {

  $result='PASS';
  $field='NULL';
  $error='NULL';

  // GlobalSettingsList-cache-threshold             - a float between 0.1 and 0.9
  if (isset($_POST['GlobalSettingsList-cache-threshold'])) {
    $t=ValidateNumericContent('float',$_POST['GlobalSettingsList-cache-threshold'],0.1,0.9);
    if (!$t) {
      $result='FAIL';
      $field='GlobalSettingsList-cache-threshold';
      $error='cache-threshold must be a float between 0.1 and 0.9';
    }
  }
  // GlobalSettingsList-hold-reclaimed-time         - an integer > -1
  if (isset($_POST['GlobalSettingsList-hold-reclaimed-time'])) {
    $t=ValidateNumericContent('integer',$_POST['GlobalSettingsList-hold-reclaimed-time'],0,false);
    if (!$t) {
      $result='FAIL';
      $field='GlobalSettingsList-hold-reclaimed-time';
      $error='hold-reclaimed-time must be an integer and must be > -1';
    }
  }
  // GlobalSettingsList-valid-lifetime              - an integer > 0
  if (isset($_POST['GlobalSettingsList-valid-lifetime'])) {
    $t=ValidateNumericContent('integer',$_POST['GlobalSettingsList-valid-lifetime'],1,false);
    if (!$t) {
      $result='FAIL';
      $field='GlobalSettingsList-valid-lifetime';
      $error='valid-lifetime must be an integer and must be > 0';
    }
  }
  // GlobalSettingsList-ddns-generated-prefix       - a valid dns label string
    // use DomainNameValidate();
  if (isset($_POST['GlobalSettingsList-ddns-generated-prefix'])) {
    list($t,$msg)=DomainNameValidate($_POST['GlobalSettingsList-ddns-generated-prefix']);
    if (!$t) {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-generated-prefix';
      $error='ddns-generated-prefix must contain one or more valid DNS labels';
    }
  }
  // GlobalSettingsList-ddns-qualifying-suffix      - a valid dns label string
    // use DomainNameValidate();
  if (isset($_POST['GlobalSettingsList-ddns-qualifying-suffix'])) {
    list($t,$msg)=DomainNameValidate($_POST['GlobalSettingsList-ddns-qualifying-suffix']);
    if (!$t) {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-qualifying-suffix';
      $error='ddns-qualifying-suffix must contain one or more valid DNS labels';
    }
  }
  // GlobalSettingsList-ddns-replace-client-name    - always or never
  if (isset($_POST['GlobalSettingsList-ddns-replace-client-name'])) {
    if ($_POST['GlobalSettingsList-ddns-replace-client-name'] != 'always' &&
        $_POST['GlobalSettingsList-ddns-replace-client-name'] != 'never') {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-replace-client-name';
      $error='ddns-replace-client-name must contain a value of always or never';
    }
  }
  // GlobalSettingsList-ddns-send-updates           - true or false
  if (isset($_POST['GlobalSettingsList-ddns-send-updates'])) {
    if ($_POST['GlobalSettingsList-ddns-send-updates'] != 'true' &&
        $_POST['GlobalSettingsList-ddns-send-updates'] != 'false') {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-send-updates';
      $error='ddns-send-updates must contain a value of true or false';
    }
  }
  // GlobalSettingsList-authoritative               - true or false
  if (isset($_POST['GlobalSettingsList-authoritative'])) {
    if ($_POST['GlobalSettingsList-authoritative'] != 'true' &&
        $_POST['GlobalSettingsList-authoritative'] != 'false') {
      $result='FAIL';
      $field='GlobalSettingsList-authoritative';
      $error='authoritative must contain a value of true or false';
    }
  }
  // GlobalSettingsList-ddns-override-client-update - true or false
  if (isset($_POST['GlobalSettingsList-ddns-override-client-update'])) {
    if ($_POST['GlobalSettingsList-ddns-override-client-update'] != 'true' &&
        $_POST['GlobalSettingsList-ddns-override-client-update'] != 'false') {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-override-client-update';
      $error='ddns-override-client-update must contain a value of true or false';
    }
  }
  // GlobalSettingsList-echo-client-id              - true or false
  if (isset($_POST['GlobalSettingsList-echo-client-id'])) {
    if ($_POST['GlobalSettingsList-echo-client-id'] != 'true' &&
        $_POST['GlobalSettingsList-echo-client-id'] != 'false') {
      $result='FAIL';
      $field='GlobalSettingsList-echo-client-id';
      $error='echo-client-id must contain a value of true or false';
    }
  }
  // GlobalSettingsList-ddns-override-no-update     - true or false
  if (isset($_POST['GlobalSettingsList-ddns-override-no-update'])) {
    if ($_POST['GlobalSettingsList-ddns-override-no-update'] != 'true' &&
        $_POST['GlobalSettingsList-ddns-override-no-update'] != 'false') {
      $result='FAIL';
      $field='GlobalSettingsList-ddns-override-no-update';
      $error='ddns-override-no-update must contain a value of true or false';
    }
  }

  return(array($result,$field,$error));
}

