<?php

function SharedNetworksBuild($arr,$sharedNetworksArray,$confType) {
  // dhcp6 not yet supported
  if ($confType=='dhcp6') {
    error("DHCPv6 configurations not yet supported.");
  }
  // iterate incoming $sharedNetworksArray adding content to $content
  // create content array
  $content=array();
  foreach ($sharedNetworksArray as $SharedNetworkName => $array) {
    $subnets=array();
    foreach ($array as $key => $subnet) {
      $subnets[$key]['subnet']=$subnet['subnet'];
      $subnets[$key]['id']=$subnet['id'];
      // add option-data for router
      if ($subnet['router']) {
        $subnets[$key]['option-data'][]=array(
          'name' => 'routers',
          'data' => $subnet['router']
        );
      }
      // add one or more pools
      foreach ($array[$key]['pools'] as $key2 => $pool) {
        $subnets[$key]['pools'][]=$pool;
      }
    }
    if (!empty($subnets)) {
      // we have both a shared network and at least one subnet
      $content[] = array(
        'name' => $SharedNetworkName,
        'subnet4' => $subnets
      );
    }
  }
  if (!empty($content)) {
    // there was some content - add to shared-networks section
    $arr['shared-networks']=$content;
  }

  // return properly formatted $arr with new Shared Networks section
  return($arr);
}
