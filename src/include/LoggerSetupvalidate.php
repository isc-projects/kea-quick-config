<?php

// validate LoggerSetupForm content
// This initially will be very simple
// only need to validate that a valid looking
// filesystem location was specified
// This can be optional.  If not specified
// then stdout will be used

function LoggerSetupvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';
  if (!empty($_POST['loggingDirectory'])) {
    // it is allowed to be empty so we only check if it isn't empty
    // we need to make sure it is a valid file location
    // though we can only validate the format
    list($result,$error)=FilePathValidate($_POST['loggingDirectory'],true);
    if ($result=='FAIL') {
      $field='loggingDirectory';
    }
  }
  return(array($result,$field,$error));
}

