<?php

// validate ConfTypeChooserForm content
// not much here other than selecting conf type

function ConfTypeChooservalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';
  if ($_POST['ConfType']=='dhcp6') {
    // DHCPv6 not currently supported
    $result='FAIL';
    $field='ConfType';
    $error='DHCPv6 is not currently supported';
  }
  return(array($result,$field,$error));
}
