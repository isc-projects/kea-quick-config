<?php


// Function to determine if an IPv4 address is actually a valid IPv4 address
function validIP($ip) {
  // set return to false by default
  $r=false;
  $comp4=filter_var($ip,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV4));
  $comp6=filter_var($ip,FILTER_VALIDATE_IP,array('flags' => FILTER_FLAG_IPV6));
  if ($ip==$comp4 || $ip==$comp6) {
    $r=true;
  }
  // return the result
  return($r);
}
