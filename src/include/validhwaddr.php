<?php

/*
This function checks if a hwaddr/mac is valid.
If is valid, true is returned, else false.
*/

function validhwaddr($hwaddr) {
  // we only support one format of mac in this software:
  // 00:00:00:00:00:00
  // turn it lower case first for simplicity
  $hwaddr=strtolower($hwaddr);
  // the allowable characters
  $c='[a-f0-9]';
  // the stupid looking match
  $match='/^'.$c.$c.':'.$c.$c.':'.$c.$c.':'.$c.$c.':'.$c.$c.':'.$c.$c.'$/';
  if (preg_match($match,$hwaddr)) {
    // it was a hwaddr of the correct format!
    return(true);
  } else {
    // the format was incorrect somehow
    return(false);
  }
}
