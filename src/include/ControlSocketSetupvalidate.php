<?php

// validate ControlSocketSetupForm content
// not much here other than control-socket being a valid UNIX file location if exist

function ControlSocketSetupvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';
  if (!empty($_POST['socket'])) {
    // There was content in the control-socket box, let us validate if it is 
    // a valid file location
    list($result,$error)=FilePathValidate($_POST['socket']);
    if ($result == 'FAIL') {
      $field='socket';
    }
  }
  return(array($result,$field,$error));
}
