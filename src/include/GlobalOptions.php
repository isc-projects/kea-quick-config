<?php

/*
  This function takes one or more Global Options from $_POST
  and adds them to the configuration array.  No need to validate
  as that was already done.
*/

function GlobalOptions($arr,$prefix=false) {
  // need to create an array here to iterate as this is slightly more
  // complicated that in the global settings as the options need to be
  // inserted into an option-data block
  $optionsFD=array(
    $prefix.'GlobalOptionsList-domain-name-servers',
    $prefix.'GlobalOptionsList-domain-name',
    $prefix.'GlobalOptionsList-ntp-servers',
    $prefix.'GlobalOptionsList-time-offset',
    $prefix.'GlobalOptionsList-time-servers',
    $prefix.'GlobalOptionsList-static-routes',
    $prefix.'GlobalOptionsList-tftp-server-name',
    $prefix.'GlobalOptionsList-boot-file-name'
  );
  // an array that will contain the iterated options
  $option_data=array();
  $count=0;
  foreach ($optionsFD as $key => $name) {
    if (isset($_POST[$name])) {
      $data=$_POST[$name];
      if ($prefix) {
        // substr start count needed for strip of $prefix from $name
        $start=strlen($prefix);
        $name=substr($name,$start);
      }
      $name=substr($name,18);
      $option_data[$count]['name']=$name;
      $option_data[$count]['data']=$data;
      $count++;
    }
  }
  // If something was added to the $option_data array, then we should
  // attach that to $arr
  if (array_key_exists(0,$option_data)) {
    $arr["option-data"]=$option_data;
  }
  return($arr);
}
