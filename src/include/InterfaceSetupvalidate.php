<?php

// validate InterfaceSetupForm content
// not much here other than interface selection to validate

function InterfaceSetupvalidate() {
  // defaults
  $result='PASS';
  $field='NULL';
  $error='NULL';

  if (empty($_POST['interfaces'])) {
    // if the interfaces-config box is empty, that is an error
    $result='FAIL';
    $field='interfaces';
    $error='The interfaces-config is required';
  } else {
    $array=explode(',',$_POST['interfaces']);
    foreach ($array as $key => $value) {
      if (preg_match('/^[A-Za-z0-9]+$/',$value) || preg_match('/^\*$/',$value)) {
        // The interface should contain one or more [A-Za-z0-9] comma separated
        // or it may contain a *
      } else {
        // anything else should be an error
        $result='FAIL';
        $field='interfaces';
        $error='The interfaces-config may contain entries like * or enso2 or eth0 but not: '.$value;
      }
    }
  }
  return(array($result,$field,$error));
}
