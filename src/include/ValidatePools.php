<?php

// Validate pools by submitted data
// there could be several pools of the format
// $_POST['pooln-z']
// where n was submitted to the function
// and z is an incremented number for each 
// added pool instance
// this function is specific to SubnetSetupvalidate()

function ValidatePools($router,$subnet,$n,$type) {
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // IPv6 not yet supported
  if ($type=='IPv6') {
    $result='FAIL';
    $field='ARMLINK';
    $error='DHCPv6 is not currently supported';
  }
  // need array of found pools so we can make sure none overlap
  $FoundPools = array();
  // lets run through the one or more pools submitted
  // for this subnet
  foreach ($_POST as $key => $value) {
    $match='pool'.$n;
    if (preg_match("/^$match/",$key)) {
      // break the component parts into a begin an end ip
      $parts=explode('-',$value);
      if (is_array($parts) && count($parts)==2) {
        // strip the whitespace
        $parts[0]=trim($parts[0]);
        $parts[1]=trim($parts[1]);
        // validate that each part is an IP
        if (!validIP($parts[0]) || !validIP($parts[1])) {
          $result='FAIL';
          $field=$key;
          $error=$value.' contains an invalid IP address';
        }
        // validate that both parts belong in submitted subnet
        if (!InSubnet($parts[0],$subnet) || !InSubnet($parts[1],$subnet)) {
          $result='FAIL';
          $field=$key;
          $error=$value.' contains an IP address outside of subnet '.$subnet;
        }
        if ($type=='IPv4') {
          // validate that each parts[0] comes before parts[1] (easier in ipv4 than ipv6)
          if (ip2long($parts[0]) > ip2long($parts[1])) {
            $result='FAIL';
            $field=$key;
            $error='Beginning of pool is larger than end of pool';
          }
          // validate that router either comes before parts[0] or after parts[1] (easier in ipv4 than ipv6)
          if (ip2long($router) >= ip2long($parts[0]) && ip2long($router) <= ip2long($parts[1])) {
            $result='FAIL';
            $field=$key;
            $error='Router '.$router.' falls inside dynamic pool '.$value;
          }
          if ($result=='PASS') {
            $FoundPools[]="$key,$value";
          }
          // here we should be validating poolClientClass'+sid+'-'+pid but cannot
          // one client class has been selected here
          // Need to confirm that they match something
          // except we cannot because the class names are not available in this form input
          // a major architectural change would be needed to support this validation
          // the validation shouldn't be strictly necessary as the content should come from
          // a dynamic select box.  If the content submitted does not match a configured class,
          // the worst case is a broken configuration.  Therefore this validation shall be skipped.
        } else if ($type=='IPv6') {
          // IPv6 not yet supported
          $result='FAIL';
          $field=$key;
          $error='IPv6 not yet supported';
          // validate that each parts[0] comes before parts[1] (easier in ipv4 than ipv6)
          // validate that router either comes before parts[0] or after parts[1] (easier in ipv4 than ipv6)
        }
      } else {
        $result='FAIL';
        $field=$key;
        $error=$value.' is not a valid pool';
      }
    }
  }
  // a final check here.  Make sure that none of the checked pools overlap
  $starts = array();
  $stops = array();
  foreach ($FoundPools as $key => $rec) {
    // get hte field and pool and parts
    list($tmp,$pool)=explode(',',$rec);
    $parts=explode('-',$pool);
    $parts[0]=trim($parts[0]);
    $parts[1]=trim($parts[1]);
    // turn the IPs into start and stop integers
    $start=ip2long($parts[0]);
    $stop=ip2long($parts[1]);
    foreach ($starts as $key => $x) {
      $y=$stops[$key];
      // check if this pool steps on any of the previously evaluated pools
      if (($start >= $x && $start <= $y) || ($stop >= $x && $stop <= $y)) {
        $result='FAIL';
        $field=$tmp;
        $error='Pool '.$pool.' overlaps some other pool '.long2ip($x).' - '.long2ip($y);
      }
    }
    // add these to the array for checking the next pool
    $starts[]=$start;
    $stops[]=$stop;
  }
  return(array($result,$field,$error));
}
