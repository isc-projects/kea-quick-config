<?php

// This include file is meant to close the  HTML </body></html>
function close() {
  // $output is the variable that will be filled with things and printed at the end
  global $output;

  $output.="\n\n<div id=VersionInfo class=test>Kea Quick Config version: <a href=# onclick=\"showRelnotes('VersionPortal','VersionPortalClose')\">0.5</a></div>\n";
  $output.="<div id=PreventClicks><div id=StartOverConfirm>";
  $output.="Are you sure you want to Start Over?  This will erase all entries and start back at the beginning.<br><br>";
  $output.="<button onclick=\"ClearAllVals('CONFIRMED');\">Start Over</button>";
  $output.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  $output.="<button onclick=\"ClearAllVals('CANCELED');\">Cancel</button>";
  $output.="</div></div>\n";
  $output.="<div id=VersionPortal></div>\n";
  $output.="<div id=VersionPortalClose><a class=ANOSTYLE href=# onclick=\"showRelnotes('VersionPortal','VersionPortalClose')\"><img src=images/close-button-png-30231.png border=0 width=30 height=30></a></div>";
  $output.="<div id=KeaLogoDiv><a class=ANOSTYLE href=https://www.isc.org/kea/ target=_new><img src=images/Kea-Complete-Logo.png width=50 height=50 border=0></a></div>";
  $output.="</body>\n";
  $output.="</html>\n";

  // Thats it for now!

}
