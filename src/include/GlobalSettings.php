<?php

/*
  This function takes one or more Global Settings from $_POST
  and adds them to the configuration array.  No need to validate
  as that was already done.
*/

function GlobalSettings($arr) {
  // GlobalSettingsList-authoritative               - true or false
  if (isset($_POST['GlobalSettingsList-authoritative'])) {
    if ($_POST['GlobalSettingsList-authoritative']=='true') {
      $_POST['GlobalSettingsList-authoritative']=true;
    } else {
      $_POST['GlobalSettingsList-authoritative']=false;
    }
    settype($_POST['GlobalSettingsList-authoritative'],'bool');
    $arr['authoritative']=$_POST['GlobalSettingsList-authoritative'];
  }
  // always set cache-threshold
  $arr['cache-threshold'] = 0.25;
  // GlobalSettingsList-cache-threshold             - a float between 0.1 and 0.9
  if (isset($_POST['GlobalSettingsList-cache-threshold'])) {
    settype($_POST['GlobalSettingsList-cache-threshold'],'float');
    $arr['cache-threshold'] = $_POST['GlobalSettingsList-cache-threshold'];
  }
  // GlobalSettingsList-hold-reclaimed-time         - an integer > -1
  if (isset($_POST['GlobalSettingsList-hold-reclaimed-time'])) {
    settype($_POST['GlobalSettingsList-hold-reclaimed-time'],'integer');
    $arr['expired-leases-processing']=array(
      'hold-reclaimed-time' => $_POST['GlobalSettingsList-hold-reclaimed-time']
    );
  }
  // always set calculate-tee-times (personal preference)
  $arr['calculate-tee-times'] = true;
  // always set "decline-probation-period": 15 as a mitigation for clients sending many DHCPDECLINES
  // This will prevent the clients from tying up IP addresses for the default of 86400 seconds.
  // see https://gitlab.isc.org/isc-projects/kea-quick-config/-/issues/70 for more details.
  $arr['decline-probation-period'] = 15;
  // GlobalSettingsList-valid-lifetime              - an integer > 0
  if (isset($_POST['GlobalSettingsList-valid-lifetime'])) {
    settype($_POST['GlobalSettingsList-valid-lifetime'],'integer');
    $arr['valid-lifetime']=$_POST['GlobalSettingsList-valid-lifetime'];
  }
  // GlobalSettingsList-ddns-generated-prefix       - a valid dns label string
  if (isset($_POST['GlobalSettingsList-ddns-generated-prefix'])) {
    $arr['ddns-generated-prefix']=$_POST['GlobalSettingsList-ddns-generated-prefix'];
  }
  // GlobalSettingsList-ddns-qualifying-suffix      - a valid dns label string
    // use DomainNameValidate();
  if (isset($_POST['GlobalSettingsList-ddns-qualifying-suffix'])) {
    $arr['ddns-qualifying-suffix']=$_POST['GlobalSettingsList-ddns-qualifying-suffix'];
  }
  // GlobalSettingsList-ddns-replace-client-name    - always or never
  if (isset($_POST['GlobalSettingsList-ddns-replace-client-name'])) {
    $arr['ddns-replace-client-name']=$_POST['GlobalSettingsList-ddns-replace-client-name'];
  }
  // GlobalSettingsList-ddns-send-updates           - true or false
  if (isset($_POST['GlobalSettingsList-ddns-send-updates'])) {
    if ($_POST['GlobalSettingsList-ddns-send-updates']=='true') {
      $_POST['GlobalSettingsList-ddns-send-updates']=true;
    } else {
      $_POST['GlobalSettingsList-ddns-send-updates']=false;
    }
    settype($_POST['GlobalSettingsList-ddns-send-updates'],'bool');
    $arr['ddns-send-updates']=$_POST['GlobalSettingsList-ddns-send-updates'];
  }
  // GlobalSettingsList-ddns-override-client-update - true or false
  if (isset($_POST['GlobalSettingsList-ddns-override-client-update'])) {
    if ($_POST['GlobalSettingsList-ddns-override-client-update']=='true') {
      $_POST['GlobalSettingsList-ddns-override-client-update']=true;
    } else {
      $_POST['GlobalSettingsList-ddns-override-client-update']=false;
    }
    settype($_POST['GlobalSettingsList-ddns-override-client-update'],'bool');
    $arr['ddns-override-client-update']=$_POST['GlobalSettingsList-ddns-override-client-update'];
  }
  // GlobalSettingsList-ddns-override-no-update     - true or false
  if (isset($_POST['GlobalSettingsList-ddns-override-no-update'])) {
    if ($_POST['GlobalSettingsList-ddns-override-no-update']=='true') {
      $_POST['GlobalSettingsList-ddns-override-no-update']=true;
    } else {
      $_POST['GlobalSettingsList-ddns-override-no-update']=false;
    }
    settype($_POST['GlobalSettingsList-ddns-override-no-update'],'bool');
    $arr['ddns-override-no-update']=$_POST['GlobalSettingsList-ddns-override-no-update'];
  }
  // GlobalSettingsList-echo-client-id              - true or false
  if (isset($_POST['GlobalSettingsList-echo-client-id'])) {
    if ($_POST['GlobalSettingsList-echo-client-id']=='true') {
      $_POST['GlobalSettingsList-echo-client-id']=true;
    } else {
      $_POST['GlobalSettingsList-echo-client-id']=false;
    }
    settype($_POST['GlobalSettingsList-echo-client-id'],'bool');
    $arr['echo-client-id']=$_POST['GlobalSettingsList-echo-client-id'];
  }
  return($arr);
}
