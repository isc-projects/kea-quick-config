<?php

function InterfacesConfig($arr,$data) {
  // data should have already been validated
  // return the 'interfaces-config' portion
  $interfaces=explode(',',$data);
  $arr['interfaces-config'] = array(
    'interfaces' => $interfaces
  );
  return($arr);
}
