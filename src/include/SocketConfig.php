<?php

function SocketConfig($arr,$socket) {
  // this should have already been validated
  if ($socket) {
    $arr['control-socket'] = array(
      'socket-type' => 'unix',
      'socket-name' => $socket
    );
  }
  return($arr);
}
