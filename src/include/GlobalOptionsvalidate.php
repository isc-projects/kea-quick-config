<?php

// validate GlobalOptionsForm content
// This is a list of one or more Global options
// there will need to be a specific validation for
// most of them though there could be some overlap
// they are, for the most part, disparate options.

function GlobalOptionsvalidate() {
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // GlobalOptionsList-domain-name-servers	- a comma separated list of IPv4 addresses
  if (isset($_POST['GlobalOptionsList-domain-name-servers'])) {
    if (!_commaSepIPv4Check($_POST['GlobalOptionsList-domain-name-servers'])) {
      $result='FAIL';
      $field='GlobalOptionsList-domain-name-servers';
      $error='domain-name-servers contains an invalid IPv4 address';
    }
  }
  // GlobalOptionsList-domain-name		- a single domain name
  if (isset($_POST['GlobalOptionsList-domain-name'])) {
    list($t,$msg)=DomainNameValidate($_POST['GlobalOptionsList-domain-name']);
    if (!$t) {
      $result='FAIL';
      $field='GlobalOptionsList-domain-name';
      $error='domain-name must contain one or more valid DNS labels';
    }
  }
  // GlobalOptionsList-ntp-servers		- a comma separated list of IPv4 addresses
  if (isset($_POST['GlobalOptionsList-ntp-servers'])) {
    if (!_commaSepIPv4Check($_POST['GlobalOptionsList-ntp-servers'])) {
      $result='FAIL';
      $field='GlobalOptionsList-ntp-servers';
      $error='ntp-servers contains an invalid IPv4 address';
    }
  }
  // GlobalOptionsList-time-offset		- a signed 32bit integer
  if (isset($_POST['GlobalOptionsList-time-offset'])) {
    $t=ValidateNumericContent('integer',$_POST['GlobalOptionsList-time-offset'],-2147483647,2147483647);
    if (!$t) {
      $result='FAIL';
      $field='GlobalOptionsList-time-offset';
      $error='time-offset must be a signed 32 bit integer';
    }
  }
  // GlobalOptionsList-time-servers		- a comma separated list of IPv4 addresses
  if (isset($_POST['GlobalOptionsList-time-servers'])) {
    if (!_commaSepIPv4Check($_POST['GlobalOptionsList-time-servers'])) {
      $result='FAIL';
      $field='GlobalOptionsList-time-servers';
      $error='time-servers contains an invalid IPv4 address';
    }
  }
  // GlobalOptionsList-static-routes		- a comma separated list of IPv4 addresses
  if (isset($_POST['GlobalOptionsList-static-routes'])) {
    if (!_commaSepIPv4Check($_POST['GlobalOptionsList-static-routes'])) {
      $result='FAIL';
      $field='GlobalOptionsList-static-routes';
      $error='static-routes contains an invalid IPv4 address';
    }
  }
  // GlobalOptionsList-tftp-server-name		- an FQDN
  if (isset($_POST['GlobalOptionsList-tftp-server-name'])) {
    list($t,$msg)=DomainNameValidate($_POST['GlobalOptionsList-tftp-server-name']);
    if (!$t) {
      $result='FAIL';
      $field='GlobalOptionsList-tftp-server-name';
      $error='tftp-server-name must be a valid FQDN';
    }
  }
  // GlobalOptionsList-boot-file-name		- a string
  if (isset($_POST['GlobalOptionsList-boot-file-name'])) {
    // This is some kind of string.  We don't care what is in here.
    // This is the administratior's problem if the string is wrong.
    // The string should match a filename somewhere, we cannot predict what that is.
    // Therefore, no validation is possible.
  }
  return(array($result,$field,$error));
}

function _commaSepIPv4Check($s) {
  $list=explode(',',$s);
  foreach ($list as $key => $ip) {
    $ip=trim($ip);
    $x=is4or6($ip);
    if ($x != 'IPv4') {
      // the submitted IP was not a valid IPv4 address or was empty
      return(false);
    }
  }
  return(true);
}
