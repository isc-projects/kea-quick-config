<?php

// validate non GlobalOptionsForm content
// This is a list of one or more non Global options
// there will need to be a specific validation for
// most of them though there could be some overlap
// they are, for the most part, disparate options.
// wanted to re-use GlobalOptionsvalidate() and probably
// could have if I didn't mind a major refactoring of that
// validation.  In the end, this will be left to someone
// else who feels strongly about it.

function nonGlobalOptionsvalidate($prefix) {
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // $prefix_|NGO|_GlobalOptionsList-domain-name-servers	- a comma separated list of IPv4 addresses
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-domain-name-servers'])) {
    if (!_commaSepIPv4Check($_POST[$prefix.'_|NGO|_GlobalOptionsList-domain-name-servers'])) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-domain-name-servers';
      $error='domain-name-servers contains an invalid IPv4 address';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-domain-name		- a single domain name
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-domain-name'])) {
    list($t,$msg)=DomainNameValidate($_POST[$prefix.'_|NGO|_GlobalOptionsList-domain-name']);
    if (!$t) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-domain-name';
      $error='domain-name must contain one or more valid DNS labels';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-ntp-servers		- a comma separated list of IPv4 addresses
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-ntp-servers'])) {
    if (!_commaSepIPv4Check($_POST[$prefix.'_|NGO|_GlobalOptionsList-ntp-servers'])) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-ntp-servers';
      $error='ntp-servers contains an invalid IPv4 address';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-time-offset		- a signed 32bit integer
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-time-offset'])) {
    $t=ValidateNumericContent('integer',$_POST[$prefix.'_|NGO|_GlobalOptionsList-time-offset'],-2147483647,2147483647);
    if (!$t) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-time-offset';
      $error='time-offset must be a signed 32 bit integer';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-time-servers		- a comma separated list of IPv4 addresses
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-time-servers'])) {
    if (!_commaSepIPv4Check($_POST[$prefix.'_|NGO|_GlobalOptionsList-time-servers'])) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-time-servers';
      $error='time-servers contains an invalid IPv4 address';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-static-routes		- a comma separated list of IPv4 addresses
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-static-routes'])) {
    if (!_commaSepIPv4Check($_POST[$prefix.'_|NGO|_GlobalOptionsList-static-routes'])) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-static-routes';
      $error='static-routes contains an invalid IPv4 address';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-tftp-server-name		- an FQDN
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-tftp-server-name'])) {
    list($t,$msg)=DomainNameValidate($_POST[$prefix.'_|NGO|_GlobalOptionsList-tftp-server-name']);
    if (!$t) {
      $result='FAIL';
      $field=$prefix.'_|NGO|_GlobalOptionsList-tftp-server-name';
      $error='tftp-server-name must be a valid FQDN';
    }
  }
  // $prefix_|NGO|_GlobalOptionsList-boot-file-name		- a string
  if (isset($_POST[$prefix.'_|NGO|_GlobalOptionsList-boot-file-name'])) {
    // This is some kind of string.  We don't care what is in here.
    // This is the administratior's problem if the string is wrong.
    // The string should match a filename somewhere, we cannot predict what that is.
    // Therefore, no validation is possible.
  }
  return(array($result,$field,$error));
}
