<?php

// validate ReservationSetupForm content
// There will be one or more reservations to be validated
// They require reservation-hwaddr.$n
// They may have reservation-hostname.$n
// There may also be some options associated
// There will NOT be any ipaddress in the global, however
// as that will be required to be confined to the subnet
// Kea supports global ipaddress settings in host reservations
// but does not recommend.  As this is a simplified configuration tool,
// we won't allow.

function GlobalReservationsvalidate() {

  // default to pass so we can set specific failure conditions
  $result='PASS';
  $field='NULL';
  $error='NULL';

  // $n is integer but will need to set type integer every time :(
  $n=0;

  // find all reservation-hostname and check for non-matching reservation-hwaddr
  // as this is an error condition
  foreach ($_POST as $key => $value) {
    if (substr($key,0,20) == 'reservation-hostname') {
      // need the id part to look for a matching reservation-hwaddr.$n
      $n=substr($key,20);
      settype($n,"integer");
      if (!isset($_POST['reservation-hwaddr'.$n])) {
        $result='FAIL';
        $field=$key;
        $error='I found a hostname with no matching hw-addr';
      }
    }
  }

  // Here lets validate the options that are set in the classes.  Need to tease out the prefix and then execute the function
  $prefixChecked=array();
  foreach ($_POST as $key => $value) {
    if (preg_match('/\_\|NGO\|\_/',$key)) {
      list($prefix,$junk)=explode('_|NGO|_',$key);
      if (empty($prefixChecked[$prefix])) {
        list($a,$b,$c)=nonGlobalOptionsvalidate($prefix);
        if ($a=='FAIL') {
          $result=$a;
          $field=$b;
          $error=$c;
        }
        $prefixChecked[$prefix]=true;
      }
    }
  }

  // find all reservation-hwaddr and extract the $n part in a loop
  $used_hw_addr=array();
  foreach ($_POST as $key => $value) {
    if (substr($key,0,18) == 'reservation-hwaddr') {
      // extract the id
      $n=substr($key,18);
      settype($n,"integer");
      // validate the value of that the current reservation-hwaddr.$n is a valid mac address
      if (empty($value) || !validhwaddr($value)) {
        $result='FAIL';
        $field=$key;
        $error=$value.' is not a valid hardware address (MAC)';
      } else if (in_array($value,$used_hw_addr)) {
        // hw-addr must appear only once in each reservation section
        $result='FAIL';
        $field=$key;
        $error=$value.' must be a unique value.';
      } else {
        // remember we already saw this one
        $used_hw_addr[]=$value;
      }
      // find out if there is a reservation-hostname.$n and ensure it is valid
      if (isset($_POST['reservation-hostname'.$n])) {
        if (empty($_POST['reservation-hostname'.$n])) {
          $result='FAIL';
          $field='reservation-hostname'.$n;
          $error='If specified, the Hostname must not evaluate to empty';
        }
        list($r,$e)=DomainNameValidate($_POST['reservation-hostname'.$n]);
        if (!$r) {
          $result='FAIL';
          $field='reservation-hostname'.$n;
          $error=$e;
        }
      }
      //if (isset($_POST['reservation-client-classes'.$n])) {
        // one ore more client classes have been selected here
        // Need to confirm that they match something
        // except we cannot because the class names are not available in this form input
        // a major architectural change would be needed to support this validation
        // the validation shouldn't be strictly necessary as the content should come from
        // a dynamic select box.  If the content submitted does not match a configured class,
        // the worst case is a broken configuration.  Therefore this validation shall be skipped.
      //}
    }
  }

  // return the result array
  return(array($result,$field,$error));

}
