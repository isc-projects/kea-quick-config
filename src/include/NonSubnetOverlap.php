<?php

// test a submitted array of subnets to see if any of them overlap
// the CIDRS list submitted is requried to contain only previously validated subnets
function NonSubnetOverlap($CIDRS) {
  // enter a foreach loop of the submitted subnets
  // init array which will contain substringed bits
  $list=array();
  foreach ($CIDRS as $key => $CIDR) {
    // split on the '/' for $net and $mask
    list($net,$mask)=explode('/',$CIDR);
    settype($mask,"integer");
    // get the bits
    $bin=bits($net);
    // substring the bits 0,$mask
    $chopped=substr($bin,0,$mask);
    // check array entries against this substringed bits
    foreach ($list as $key => $entry) {
      // the subnet bits may be longer or shorder depending on mask.
      // we need to compare equal segments so we have some substr below 
      // after measuring the comparative lengths
      $x=$chopped;
      if (strlen($entry) > strlen($x)) {
        $entry=substr($entry,0,$mask);
      } else if (strlen($entry) < strlen($x)) {
        $n=strlen($entry);
        $x=substr($x,0,$n);
      }
      // if any matches are found, return(array(false,$CIDR))
      if ($x==$entry) {
        return(array(false,$CIDR));
      }
    }
    // if no matches found, add substringed bits to $list array 
    $list[]=$chopped;
  }
  // if we got this far, then nothing overlapped!
  return(array(true,null));
}
