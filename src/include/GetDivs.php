<?php

// this function creates the main body of the setup wizard
// it collects text from several text files named for the
// div they create.  I found this convenient to make HTML
// content editing easier.

function GetDivs() {
  $html='';

  // array of pages to show
  $array=array(
    'ConfTypeChooser.html',      // welcome page - choose DHCPv4 or DHCPv6 here
    'ControlSocketSetup.html',   // UNIX socket location
    'InterfaceSetup.html',       // the interface configuration
    'LeaseDatabaseSetup.html',   // the lease database configuration
    'GlobalSettings.html',       // Global settings (ddns-send-updates, echo-client-id, valid-lifetime, etc..)
    'GlobalOptions.html',        // Global options (domain-name-servers, domain-name, ntp-servers, time-offset, etc...)
    'ClientClassification.html', // Define Client classes here, as necessary
    'Reservations.html',         // Define host reservations here, as necessary
    'HooksSetup.html',           // Collect hook location and a list of hooks to load
    'SharedNetworkSetup.html',   // Define one or more shared-networks here
    'SubnetSetup.html',          // Gather subnet details (may be part of shared-network)
    'LoggerSetup.html',          // Gather detail for logging (just a directory for now)
    'theEnd.html'                // Gather all of the form elements from local storage and add as hidden items
                                 // submit to get configuration
  );
  foreach ($array as $key => $page) {
    $html.=GetContent($page);
  }
  return($html);
}
