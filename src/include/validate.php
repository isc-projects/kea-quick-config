<?php

function validate() {
  // debug
  //$x=print_r($_GET,true);
  //$y=print_r($_POST,true);
  //$stdout = fopen('php://stdout', 'w');
  //fwrite($stdout, "_GET:\n$x\n");
  //fwrite($stdout, "_POST:\n$y\n");

  // default values and return
  if (empty($_GET['form'])) {
    $_GET['form']='';
  }

  // Some charcaters cause Kea to not parse the config
  // lets check for those and return an error if present
  list($result,$field,$error,$invalidC)=CheckForInvalidCharacters();
  if ($invalidC) {
    // the above check failed - perform no further checks
  } else if ($_GET['form']=='ConfTypeChooserForm') {
    // this will start to do something now beginning with ConfTypeChooser
    list($result,$field,$error)=ConfTypeChooservalidate();
  } else if ($_GET['form']=='ControlSocketSetupForm') {
    list($result,$field,$error)=ControlSocketSetupvalidate();
  } else if ($_GET['form']=='InterfaceSetupForm') {
    list($result,$field,$error)=InterfaceSetupvalidate();
  } else if ($_GET['form']=='LeaseDatabaseSetupForm') {
    list($result,$field,$error)=LeaseDatabaseSetupvalidate();
  } else if ($_GET['form']=='GlobalSettingsForm') {
    list($result,$field,$error)=GlobalSettingsvalidate();
  } else if ($_GET['form']=='GlobalOptionsForm') {
    list($result,$field,$error)=GlobalOptionsvalidate();
  } else if ($_GET['form']=='ClientClassificationSetupForm') {
    list($result,$field,$error)=ClientClassificationSetupvalidate();
  } else if ($_GET['form']=='ReservationSetupForm') {
    list($result,$field,$error)=GlobalReservationsvalidate();
  } else if ($_GET['form']=='SharedNetworkSetupForm') {
    list($result,$field,$error)=SharedNetworkSetupvalidate();
  } else if ($_GET['form']=='SubnetSetupForm') {
    list($result,$field,$error)=SubnetSetupvalidate();
  } else if ($_GET['form']=='LoggerSetupForm') {
    list($result,$field,$error)=LoggerSetupvalidate();
  } else if ($_GET['form']=='HooksSetupForm') {
    // Validate submitted hooks parameters
    list($result,$field,$error)=Hooksvalidate();
  } else {
    // no more steps!
  }
  // return the result, field and error to the program caller
  print $result.'_|_'.$field.'_|_'.$error;
}
