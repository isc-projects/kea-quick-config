<?php

// This uses data found in $_POST that is related to one or more hooks libraries
// that were configured during the 'Hooks Configuration' step.  This data is then added
// to the incoming $arr array in the same way that is done in other functions.
function HooksLibraries($arr) {

  // Will contain the discovered values inside a main array that will be set and returned in $arr
  $main=array();

  $path='';
  if (!empty($_POST['HooksPath'])) {
    $path=$_POST['HooksPath'];
    if (substr($path,-1)=='/') {
      $path=preg_replace('/\/$/','',$path);
    }
  }

  // foreach $_POST we will look for Hooks  substring and then add these + path to the array
  // add these to the $main array.  Should be fairly simple.  No need of validation as it already happened.
  foreach ($_POST as $key => $value) {
    if (substr($key,0,5) == 'Hooks' && $key != 'HooksPath' && $key != 'HooksServer1ip' && $key != 'HooksServer2ip') {
      // init record array
      $record=array();
      if (!empty($value)) {
        $record['library']=$path.'/'.$value;
        if ($value == 'libdhcp_ha.so') {
          // need to add the arguments to the HA hook
          $record['parameters']["high-availability"][0] = array(
                                                             "this-server-name"=>'server1',
                                                             "mode"=>'hot-standby',
                                                       );
          $record['parameters']["high-availability"][0]["peers"][0]= array(
                                                                       "name"=> "server1",
                                                                       "url"=> "http://".$_POST['HooksServer1ip'].":8000/",
                                                                       "role"=> "primary"
                                                                     );
          $record['parameters']["high-availability"][0]["peers"][1]= array(
                                                                       "name"=> "server2",
                                                                       "url"=> "http://".$_POST['HooksServer2ip'].":8000/",
                                                                       "role"=> "primary"
                                                                     );
        }
      }
      if (!empty($record)) {
        $main[]=$record;
      }
    }
  }

  // add the created array here
  if (!empty($main)) {
    $arr['hooks-libraries']=$main;
  }
  // return the building array
  return($arr);
}
