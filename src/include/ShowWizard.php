<?php

// This function shows the page that collects the information from the user which
// can then be submitted to retrieve a valid Kea configuration.
// This wizard consists of several divs that appear and disappear as if you are 
// changing pages.  Each "page" flip will validate (with php) the form entries on the 
// preceding page.  Once the last page is reached, a get configuration button is shown.
// Clicking this actually submits all of the form data to a php function that returns
// the configuration to the user (after validation of the final page, of course).
// NOTE: the window showing the config should be a read-only textarea that is 
// appropriately sized for the content (no scroll bars) it can be as long or as
// wide as it needs to be.

function ShowWizard() {

  // $output is the variable that will be filled with things and printed at the end
  global $output;

  // Gather and show the "pages"
  $output.=GetDivs();

  // show a Start Over button
  $output.="  <div class=divbottom>";
  $output.="    <div class=divstartover>";
  $output.="      <button onclick=\"ClearAllVals('permission');\">Start Over</button>";
  $output.="    </div>\n";

  // show the div that will display errors
  $output.="    <div class=diverror>";
  $output.="      <span id=error class=formInput></span>";
  $output.="    </div>\n";

  // always show a link to the ARM
  $output.="    <div class=divbottomarmlink>";
  $output.="      <span id=ARMLINKVal>";
  $output.="        <a href=https://kea.readthedocs.io/en/latest/arm/intro.html target=_new>Kea Administrator's Reference Manual</a>";
  $output.="      </span>";
  $output.="    </div>";

  // close divbottom div
  $output.="  </div>";

  // show a blank line so that the config version info and kea logo are not overlapped
  // tho this could also be accomplished by setting a color on both of those divs
  $output.="  <div class=divblank></div>";

  // close MainView div
  $output.="</div>";
}
