<?php

function LeaseDatabaseConfig($arr,$type,$name,$user,$pass) {
  // this configuration will differ slightly based on if is memfile or database server type
  // this data should have already been validated
  if ($type=='memfile') {
    if ($name) {
      $arr['lease-database']=array(
        'type' => 'memfile',
        'persist' => true,
        'name' => $name
      );
    } else {
      $arr['lease-database']=array(
        'type' => 'memfile',
        'persist' => false
      );
    }
  } else if ($type=='mysql' || $type=='postgresql') {
    $arr['lease-database']=array(
      'type' => $type,
      'name' => $name,
      'user' => $user,
      'password' => $pass
    );
  } else {
    error("Unknown lease databse type: ".$type);
  }
  // here multi-threading tweaks are performed in accordance with lease-database choice
  // and info available here: https://kea.readthedocs.io/en/kea-2.2.0/arm/dhcp4-srv.html?highlight=mutli-threading#multi-threading-settings-with-different-database-backends
  $tps = false; // thread-pool-size
  $pqs = false; // packet-queue-size
  if ($type=='memfile' && !$name) {
    // do nothing as this is memfile with no persist
  } else if ($type=='memfile' && $name) {
    // apply appropriate settings for memfile
    $tps = 4;
    $pqs = 28;
  } else if ($type=='mysql') {
    // apply appropriate settings for MySQL
    $tps = 12;
    $pqs = 792;
  } else if ($type=='postgresql') {
    // apply appropriate settings for PostgreSQL
    $tps = 8;
    $pqs = 88;
  } else {
    error("Unknown combo of $type and $name\n");
  }
  if ($tps && $pqs) {
    // there are some settings to adjust from default
    $arr['multi-threading']=array(
      'enable-multi-threading' => true,
      'thread-pool-size' => $tps,
      'packet-queue-size' => $pqs
    );
  } else {
    //error("$tps or $pqs were false\n");
  }
  return($arr);
}
