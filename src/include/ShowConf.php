<?php

// this takes as input all of the form field names and values that have been collected
// throughout the several steps. These should have already been validated so that all
// this particular thing has to do is process the input and output a JSON configuration
// for use with Kea.

// *** IMPORTANT *** don't forget to post / redirect / get in here
// *** maybe not ... might change to have a step10 div that will
// have the box that will contain the configuration and will show
// such configuration after a background submission and return
// will mean that a separate thing will be needed instead of index.php
// in the same manner that validate.php works.
// the above is indeed what happened but it isn't called step10.  Rather it is called theEnd :D

// include important files
include "CheckEmpties.php";
include "ConfTypeChoice.php";
include "SocketConfig.php";
include "InterfacesConfig.php";
include "LeaseDatabaseConfig.php";
include "GlobalOptions.php";
include "Networks.php";
include "SharedNetworksBuild.php";
include "SubnetsBuild.php";
include "Loggers.php";
include "GlobalSettings.php";
include "ClientClasses.php";
include "GlobalReservations.php";
include "HooksLibraries.php";

function ShowConf() {
  // this part will probably go away
  global $output;
  // div hack so we can reuse a function
  //$output.="<div id=ConfTypeChooser>";

  // debug
  //$x=print_r($_GET,true);
  //$y=print_r($_POST,true);
  //$stdout = fopen('php://stdout', 'w');
  //fwrite($stdout, "_GET:\n$x\n");
  //fwrite($stdout, "_POST:\n$y\n");

  // make sure things that can be empty are set for use in functions
  CheckEmpties();

  // will have separate functions for each config piece

  // create the array
  $arr=array();

  // Then set the control socket portion of config based on content of $_POST['socket']
  $arr=SocketConfig($arr,$_POST['socket']);

  // Then set the interface portion of config based on content of $_POST['interfaces']
  $arr=InterfacesConfig($arr,$_POST['interfaces']);

  // Then set the lease database portion of config based on content of 
  // $_POST['LeaseDatabaseType'],$_POST['LDname'],$_POST['LDuserName'],$_POST['LDpassWord']
  $arr=LeaseDatabaseConfig($arr,$_POST['LeaseDatabaseType'],$_POST['LDname'],$_POST['LDuserName'],$_POST['LDpassWord']);

  // Global settings
  // This list of global setting will likely expand over time and so will not be passed into the function
  $arr=GlobalSettings($arr);

  // Global options
  // Similarly to the above global settings, this list will likely expand over time and thus the list is NOT passed to the function
  $arr=GlobalOptions($arr);

  // Client Classes
  // Here we add the configured client classes as gathered on 'Define Client Classes' HTML div
  $arr=ClientClasses($arr);

  // Reservations (Global)
  // Here we add the configured global reservations as gathered on 'Define Host Reservations' HTML div
  $arr=GlobalReservations($arr);

  // hooks-libraries
  $arr=HooksLibraries($arr);

  // if any shared networks were defined, set them here based on content of 
  // if no shared networks or subnets without shared networks - set them here based on content of 
  // this should be doable in a single function maybe two but called from a third
  $arr=Networks($arr,$_POST['ConfType']);

  // Now loggers based on content of $_POST['loggingDirectory']
  $arr=Loggers($arr,$_POST['loggingDirectory'],$_POST['ConfType']);

  // Enclose the configuration by setting dhcp4 or dhcp6 based on content of $_POST['ConfType']
  $arr=ConfTypeChoice($arr,$_POST['ConfType']);

  // thats it for now!  send back the result after proper conversion to json
  //$output.="CONFIG:<br>\n";
  //$output.="<pre>";
  $output.=json_encode($arr,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
  //$output.="</pre>";

  // this part will probably go away
  // close the div in the hack
  //$output.="</div>";
}

function error($msg) {
  print "<pre>".$msg."</pre>";
  exit;
}
