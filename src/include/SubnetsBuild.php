<?php

function SubnetsBuild($arr,$subnetsArray,$confType) {
  // this one is easier than shared networks
  // once more, data should have already been validated

  // dhcp6 not yet supported
  if ($confType=='dhcp6') {
    error("DHCPv6 configurations not yet supported.");
  }

  if (!empty($subnetsArray)) {
    // iterate incoming $subnetsArray properly formatting and adding to $content
    // array which will then be added to $arr prior to return
    $content=array();
    $i=0;
    foreach ($subnetsArray as $key => $array) {
      // set the subnet
      $content[$i]['subnet']=$array['subnet'];
      // set the subnet id
      $content[$i]['id']=$array['id'];
      // Add configured options for the reservation
      $n=$array['id'];
      $prefix='Subnet'.$n.'_|NGO|_';
      $x=array();
      $x=GlobalOptions($x,$prefix);
      $content[$i] = $content[$i] + $x;
      // set the router option
      if ($array['router']) {
        $content[$i]['option-data'][]=array(
          'name' => 'routers',
          'data' => $array['router']
        );
      }
      // set the one or more included pools
      foreach ($array['pools'] as $key2 => $pool) {
        $content[$i]['pools'][]=$pool;
      }
      $i++;
    }
    // set the properly formatted content in the subnet4 section
    $arr['subnet4']=$content;
  }
  
  // return properly formatted $arr with new subnet4 section
  return($arr);

}
