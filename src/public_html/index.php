<?php

/************************************************************************ 
*  Purpose:                                                             *
*  Create a simple web interface that gathers information from the user *
*  and returns a complete Kea DHCP configuration.  This configuration   *
*  could be either for kea-dhcp4 or kea-dhcp6.  This simple interface   *
*  will use no database or storage of any kind and, indeed, should be   *
*  so simple that it can be served directly from a computer with PHP    *
*  installed but no web server by executing php -S localhost:8000 from  *
*  the root directory that contains 'index.php'                         *
************************************************************************/

// Location of include files.  Change to suit where you have stored the include files
$IncLoc='../include';

// This include file will load all functions needed for this single page application
include($IncLoc.'/basics.php');

// If there is incoming validation, do that and exit
if (!empty($_GET['validate'])) {
  validate();
  exit;
}

if (!empty($_GET['SHOWCONF'])) {
  $output='';
  ShowConf();
  print $output;
  exit;
}

// $output will be a global variable that will get filled with various HTML and things and then dumped at the end.
$output='';

// somone else will need to make this pretty with css and things
// open the page
open();

// really cool things happen in here
// the meat of the page shown here
if (!empty($_GET['SHOWCONF']) && $_GET['SHOWCONF']=='true') {
  ShowConf();
} else {
  ShowWizard();
}

// Thats it! close the page!
close();

// Now lets dump the output
print $output;
