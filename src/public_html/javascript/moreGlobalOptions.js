/*
  This function adds a global Option to the GlobalOptionsContainer
  It finds which option to add by what is selected in toAddGlobalOption
  It checks if the option is already added to the form and doesn't add if so
  It gets a data set of these options from globalOptionsList()
  It then adds the form field from the HTML stored in the list
  Finally, it sets the default value as stored in the list
  NEW in 0.3, OCID is passed into the function.  This is the id of the div
  that should be populated with the option (append).  The incoming select
  box id SBOX (so that the option that is selected can be found) is also passed.
  PREFIX contains the prefix text of the option (e.g., ClientClass) which
  will be empty in the case of global options, of course.
*/
function moreGlobalOptions(iNAME,OCID,SBOX,PREFIX) {
  // what option are we to add?
  // get the currently selected options value which will match the name
  // of some option from list
  if (!OCID) {
    OCID = 'GlobalOptionsContainer';
  }
  let optionSelectedValue = '';
  if (!SBOX) {
    selectID='toAddGlobalOption';
  } else {
    selectID=SBOX;
  }
  if (!iNAME) {
    let elem = document.getElementById(selectID);
    let eSi = elem.options.selectedIndex;
    optionSelectedValue = elem.options[eSi].value;
  } else {
    optionSelectedValue = iNAME;
  }
  // just return if this is blank
  if (!optionSelectedValue) {
    return false;
  }
  // was this option already added?
  if (document.getElementById(optionSelectedValue)) {
    // Ignore if so
  } else {
    // add the form field to the form if not
    // get the list of Global Options
    const list = globalOptionsList(PREFIX);
    // Get the HTML from the list
    for (let i = 0; i < list.length; i++) {
      if (list[i].Name == optionSelectedValue) {
        // add the HTML to the select box
        let txt = '<div class=divrow>'+list[i].HTML+'</div>';
        let newDIV = document.createElement("div");
        newDIV.innerHTML = txt;
        if (document.getElementById(OCID)) {
          document.getElementById(OCID).appendChild(newDIV);

          // add the URL to the ARM as well
          txt = '<div class=divrow><a href="'+list[i].ARMurl+'" target=_new>'+list[i].ARMtitle+'</a></div>';
          newDIV = document.createElement("div");
          newDIV.innerHTML = txt;
          document.getElementById(OCID).appendChild(newDIV);

          // add separation of some kind
          txt = '<div class=divrow>---</div>';
          newDIV = document.createElement("div");
          newDIV.innerHTML = txt;
          document.getElementById(OCID).appendChild(newDIV);

          // now set the default value as defined in the list
          let defaultValue = list[i].Default;
          // Get the form field element
          // if there are no checkbox or radio (and there won't be so far)
          // can just set the value which will change what is selected
          document.getElementById(optionSelectedValue).value = defaultValue;
          // Now set the Global Options box back to a blank value to prevent
          // saving of a value in the box
          document.getElementById(selectID).value = '';
        }
      }
    }
  }
}
