function ServerComm(str,url,type) {
  // incoming str is a uriencoded key/value pair list
  // like: fname=bob&lname=smith&age=21&height=42
  // url contains the URL to which to send the pairs
  let result='';
  if (str || type=='GET') {
    // Lets send these key=value pairs
    let xhttp = new XMLHttpRequest();
    // This anonymous function gets the return if available
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        result=this.responseText;
      }
    };
    // send the traffic using a blocking method
    // this blocking method is deprecated (for a while now)
    // if it ever really is removed, a while loop will be needed
    // after the send to watch for the resposne to be filled in
    // or perhaps something with setTimeout / setInterval
    xhttp.open(type, url, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    if (type == 'GET') {
      xhttp.send();
    } else if (type == 'POST') {
      xhttp.send(str);
    }
  } else {
    // there were no key/value pairs!
    document.getElementById("error").innerHTML = "<p class=error>Not submitting empty form</p>";
  }
  // return the (possibly empty) string
  return result;
}
