// Populate entries in shared network select box in SubnetSetup
function populateSharedNetworks(selectID) {
  // This needs to be executed onclick which means it might bet executed multiple
  // times.  Will need to detect if the option already exists before adding the
  // option.  Will also need to detect what is already stored in session storage
  // and set that to selected ... but only if some other thing isn't ALREADY selected

  // get element object for incomings selectID
  let elem = document.getElementById(selectID);
  // Get a list of currently configured options so we can avoid adding duplicates
  const options = elem.options;
  // Get the currently selected (if any) option so we can avoid resetting what was selected
  let eSi = elem.options.selectedIndex
  let optionSelected = elem.options[eSi]

  // gather stored session data
  let Keys = Object.keys(sessionStorage);
  // now put them in the order the user added them in
  Keys.sort();
  // create container array for storage of values found from SharedNetworkSetup
  const SharedNetworkSetupvalues = [];
  // loop through the stored session data
  for (let i = 0; i < Keys.length; i++) {
    // if the stored session data key starts with sharedNetwork
    if (Keys[i].substr(0,13) == 'sharedNetwork') {
      // get the value of the key
      let value = ReadItem(Keys[i]);

      // add value to array of shared networks from SharedNetworkSetup
      SharedNetworkSetupvalues.push(value);

      // Find out if the option already exists and skip if so by setting proceed to false
      let proceed = true;
      for (let n = 0; n < options.length; n++) {
        if (options[n].value == value) {
          proceed = false;
        }
      }

      if (proceed) {
        // then set that as a selectable option in the select box identified by selectID
        // create option
        let option = document.createElement("option");
        // add option value
        option.value = value;
        // add text of the option
        option.text = value;
        // add the option to the previously found select box element
        elem.add(option);
      }
    }
  }

  // remove options that shouldn't exist (i.e., they were not in SharedNetworkSetup anymore)
  // iterate through the current options
  for (let n = 0; n < options.length; n++) {
    // check if their values exist in array created while iterating the shared networks from SharedNetworkSetup
    function InArray(s) {
      if (s == options[n].value) {
        return true;
      } else {
        return false;
      }
    }
    if (SharedNetworkSetupvalues.find(InArray)) {
      // do nothing, the option was found in the array
    } else {
      if (options[n].value) {
        // delete the option if not in that array and not empty
        options.remove(n);
      }
    }
  }

  // now lets set something selected, if something valid isn't already selected
  if (eSi < 1) {
    // the selected option is either 0 (the default empty option) or -1 (nothing)
    // lets get the value
    let elemValue = ReadItem(elem.name);
    // if it exists we will find the option and mark that selected, if it does not, then 0 will remain selected
    elem.options.selectedIndex = 0;
    if (elemValue) {
      // we need to get the index of the stored item so we can set that option to selected O.o
      for (let n = 0; n < options.length; n++) {
        if (options[n].value==elemValue) {
          elem.options.selectedIndex = n;
        }
      }
    }
  }

  // just return true ... we won't detect failures here
  return true;
}
