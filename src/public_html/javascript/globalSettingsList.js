/*
  This particular function creates an array with details of the global settings.
  This array is returned to the caller and the array contents are used to show
  the settings as well as populate a list of possible settings.
  The contents of the array are objects that describe the setting.

  Each object needs to contain the setting name, the type of value, the default
  value, and the HTML used for the setting.
  Also, the object should contain indicators of levels it is allowed.
  These indicators are for possbile future use.
*/
function globalSettingsList() {
  // string of HTML
  let tHTML = '';
  let fid = '';
  let sid = '';
  let fNAME = '';
  // container array
  const x = [];

  // Global settings objects
  // authoritative
  fName='GlobalSettingsList-authoritative';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Authoritative:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="true">true</option>'+
         '        <option value="false">false</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const authoritative = {
    Name:fName,
    Type:"bool",
    Default:"false",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#authoritative-dhcpv4-server-behavior',
    ARMtitle:'8.2.23. Authoritative DHCPv4 Server Behavior',
    Global:true,
    sharedNetwork:true,
    Subnet:true,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-send-updates
  fName='GlobalSettingsList-ddns-send-updates';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Send Updates:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="true">true</option>'+
         '        <option value="false">false</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const ddnsSendUpdates = {
    Name:fName,
    Type:"bool",
    Default:"false",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-qualifying-suffix
  fName='GlobalSettingsList-ddns-qualifying-suffix';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Qualifying Suffix:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder=example.com>'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const ddnsQualifyingSuffix = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-override-no-update
  fName='GlobalSettingsList-ddns-override-no-update';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Override No Update:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="true">true</option>'+
         '        <option value="false">false</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const ddnsOverrideNoUpdate = {
    Name:fName,
    Type:"bool",
    Default:"false",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-override-client-update
  fName='GlobalSettingsList-ddns-override-client-update';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Override Client Update:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="true">true</option>'+
         '        <option value="false">false</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const ddnsOverrideClientUpdate = {
    Name:fName,
    Type:"bool",
    Default:"false",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-replace-client-name
  fName='GlobalSettingsList-ddns-replace-client-name';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Replace Client Name:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="always">always</option>'+
         '        <option value="never">never</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const ddnsReplaceClientName = {
    Name:fName,
    Type:"bool",
    Default:"never",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // ddns-generated-prefix
  fName='GlobalSettingsList-ddns-generated-prefix';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>DDNS Generated Prefix:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder=myhost>'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const ddnsGeneratedPrefix = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#ddns-for-dhcpv4',
    ARMtitle:'8.2.19. DDNS for DHCPv4',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // echo-client-id
  fName='GlobalSettingsList-echo-client-id';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Echo Client ID:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <select id='+fid+' name='+fName+'>'+
         '        <option value="true">true</option>'+
         '        <option value="false">false</option>'+
         '      </select>'+
         '    </span>'+
         '  </div>';
  const EchoClientID = {
    Name:fName,
    Type:"bool",
    Default:"true",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#echoing-client-id-rfc-6842',
    ARMtitle:'8.2.21. Echoing Client-ID (RFC 6842)',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // cache-threshold
  //fName='GlobalSettingsList-cache-threshold';
  //fid=fName;
  //sid=fName+'Val';
  //tHTML = '  <div class=divlabel>'+
  //       '    <label for='+fid+' class=labelpad><small>Cache Threshold:</small></label>'+
  //       '  </div>'+
  //       '  <div class=divfield>'+
  //       '    <span id='+sid+' class=formInput>'+
  //       '      <input type=text id='+fid+' name='+fName+'>'+
  //       '      </input>'+
  //       '    </span>'+
  //       '  </div>';
  //const CacheThreshold = {
  //  Name:fName,
  //  Type:"string",
  //  Default:"0.25",
  //  HTML:tHTML,
  //  ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/dhcp4-srv.html#lease-caching',
  //  ARMtitle:'8.2.30. Lease Caching',
  //  Global:true,
  //  sharedNetwork:false,
  //  Subnet:false,
  //  Options:false,
  //  Reservations:false,
  //  Pool:false
  //};

  // expired-leases-processing{hold-reclaimed-time} *
  //   * will probably just be called hold-reclaimed-time and will be created
  //   as part of expired-leases-processing{} during configuration rendering
  fName='GlobalSettingsList-hold-reclaimed-time';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Hold Reclaimed Time:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+'>'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const HoldReclaimedTime = {
    Name:fName,
    Type:"string",
    Default:"3600",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/lease-expiration.html#lease-reclamation-configuration-parameters',
    ARMtitle:'11.2. Lease Reclamation Configuration Parameters',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // valid-lifetime
  fName='GlobalSettingsList-valid-lifetime';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Valid Lifetime:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+'>'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const ValidLifetime = {
    Name:fName,
    Type:"string",
    Default:"7200",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.0/arm/lease-expiration.html',
    ARMtitle:'11. Lease Expiration',
    Global:true,
    sharedNetwork:false,
    Subnet:false,
    Options:false,
    Reservations:false,
    Pool:false
  };

  // add the objects defined above to the returned list in the desired order
  x[0] = authoritative;
  x[1] = ValidLifetime;
  //x[2] = CacheThreshold;
  x[2] = HoldReclaimedTime;
  x[3] = EchoClientID;

  x[4] = ddnsSendUpdates;
  x[5] = ddnsOverrideNoUpdate;
  x[6] = ddnsOverrideClientUpdate;

  x[7] = ddnsReplaceClientName;
  x[8] = ddnsGeneratedPrefix;
  x[9] = ddnsQualifyingSuffix;

  return x;
}

