// This function is executed in the last step to populate the form
// with all of the collected data right before submission to
// getConfiguration.php which will return the Kea configuration
// based on the gathered form data
function AddAllFormData(formID) {
  // form id that shall be populated with the key/value hidden
  // elements is supplied as input var formID
  // This should be pretty simple
  // First, get an element that is the form to be modified with incoming formID
  const formElem = document.getElementById(formID);
  // console.log(formElem);
  // First get the keys from sessionStorage
  let Keys = Object.keys(sessionStorage);
  // then iterate through the list
  for (let i = 0; i < Keys.length; i++) {
    // retrieve alue
    let value = ReadItem(Keys[i]);
    // now set the key value as hidden form field in fromID
    let x = document.createElement("INPUT");
    x.setAttribute("type","hidden");
    x.setAttribute("name",Keys[i]);
    x.setAttribute("value",value);
    formElem.appendChild(x);
  }
}
