function moreSubnets(n) {
  // n = numeric portion of subnet id (e.g. if 1 is submitted, subnet1, would be the id of the subnet input) (required)
  if (!n) {
    return;
  }
  // n may not be an integer depending how it was retreived
  n = Number(n);
  // create next id to be sent with more executions of moreSubnets(n);
  let nextN = n+1;
  // create subnetSharedNetworkSelect1 id
  let SNSID = 'subnetSharedNetworkSelect'+n;
  // check if the above id already exists ... return; if it does
  if (document.getElementById(SNSID)) {
    return;
  }
  // create subnetSharedNetworkSelect1Val span id
  let SNSSPID = 'subnetSharedNetworkSelect'+n+'Val';
  // create subnetSharedNetworkSelect1 name
  let SNSN = SNSID;
  // create subnet1 id
  let SNID = 'subnet'+n;
  // create subnet1Val span id
  let SNSPID = 'subnet'+n+'Val';
  // create subnet1 name
  let SNN = SNID;
  // create router1 id
  let RID = 'router'+n;
  // create router1Val span id
  let RSPID = 'router'+n+'Val';
  // create router1 name
  let RN = RID;
  // create NewSubnet1 span id
  let MSSPID = 'NewSubnet';
  // create poolContainer1 id
  let PCID = 'poolContainer'+n;

  // create optionContainer1 id
  let OCID = 'SubnetoptionContainer'+n;

  // create the div object
  let newDIV = document.createElement("div");
  // create array of lines of html
  const lines = [];
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+SNSID+' class=labelpad>Shared Network:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+SNSSPID+' class=formInput>');
  lines.push('      <select class=subnetSharedNetworkSelect id='+SNSID+' name='+SNSN+' onfocus=populateSharedNetworks(this.id)>');
  lines.push('        <option></option>');
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+SNID+' class=labelpad>Subnet:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+SNSPID+' class=formInput>');
  lines.push('      <input type=text id='+SNID+' name='+SNN+' value=\'\' placeholder=\'e.g. 192.0.2.0/24\'>');
  lines.push('    </span>');
  lines.push('    <span id='+MSSPID+'>');
  lines.push('      <a class=ANOSTYLE href=# onclick="moreSubnets('+nextN+');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  lines.push('<div id='+PCID+'>');
  lines.push('</div>');
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+RID+' class=labelpad>Router:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+RSPID+' class=formInput>');
  lines.push('      <input type=text id='+RID+' name='+RN+' value=\'\' placeholder=\'e.g. 192.0.2.1\'>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // The add option select box
  lines.push('<!--<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for=toAddSubnetOption'+n+' class=labelpad>Add Subnet Option:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id=toAddSubnetOption'+n+'Val class=formInput>');
  lines.push('      <select id=toAddSubnetOption'+n+' name=toAddSubnetOption'+n+' onfocus="GlobalOptionsSelectBoxPopulate(this.id,\'Subnet'+n+'\');">');
  lines.push('        <option value=>Select...</option>');
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('    <span id=toAddSubnetOption'+n+'IMG>');
  lines.push('      <a href=# class=ANOSTYLE onclick="moreGlobalOptions(false,\''+OCID+'\',\'toAddSubnetOption'+n+'\',\'Subnet'+n+'\');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>-->');
  // the container div that will house the options added to this client class :flushed:
  lines.push('<div id='+OCID+'>');
  lines.push('</div>');
  // create txt record from array
  let txt = '';
  if (n > 1) {
    txt = txt.concat('<div><hr></div>');
  }
  for (i = 0; i < lines.length; i++) {
    txt = txt.concat(lines[i]);
  }
  // set the innerhtml of the object to the txt record
  newDIV.innerHTML = txt;
  // remove the current moreSubnets span
  const element = document.getElementById(MSSPID);
  if (element) {
    element.remove();
  }
  // append the object to the container div
  document.getElementById('subnetContainer').appendChild(newDIV);
  // execute morePools(n,1); to add the first pool
  morePools(n,1);
}
