// Update select boxes that contain classes with any classes that might have been added or removed.
function updateClassSelects() {
  // We will need a list of currently configured classes
  const classes = GetConfiguredClasses();

  // Get list of class select boxes created by moreReservations() or morePools()
  // Iterate list of select boxes gathered previously
  // In each select box find out what classes are listed there
  // Add missing classes
  // remove classes that no longer exist (except specials like KNOWN / UNKNOWN)

  // Create storage for the retrieved element IDs
  const IDs = [];
  // Get a list of IDs by matching first part of name
  let selects = document.getElementsByTagName("select");
  for(let i = 0; i < selects.length; i++){
    if (selects[i].id.substr(0,26) == 'reservation-client-classes' || selects[i].id.substr(0,15) == 'poolClientClass') {
      // add to the list of IDs for processing later
      IDs.push(selects[i].id);
    }
  }
  // iterate IDs
  for (let i=0; i < IDs.length; i++) {
    // get the element by the current id
    let curr = document.getElementById(IDs[i]);
    // iterate the curr.options
    let currOptions = curr.options;
    for (let n=0; n < currOptions.length; n++) {
      // local search function
      function InArray(s) {
        if (s == currOptions[n].value) {
          return true;
        } else {
          return false;
        }
      }
      if (classes.find(InArray)) {
        // are they in classes array? yes
      } else if (currOptions[n].value == '' || currOptions[n].value == 'KNOWN' || currOptions[n].value == 'UNKNOWN') {
        // some of them are special and should be left alone
      } else {
        // remove from curr options if not
        //currOptions.remove(n);
        console.log('I should remove'+currOptions[n].value);
        currOptions.remove(n);
      }
    }
    // iterate classes array for the currOptions[n].value
    for (let x=0; x < classes.length; x++) {
      // foreach classes[x], iterate currOptions
      let found = false;
      for (let n=0; n < currOptions.length; n++) {
        // Does the value match?
        if (classes[x] == currOptions[n].value) {
          // it was found - no need to add
          found=true;
        }
      }
      if (!found) {
        // create option
        let option = document.createElement("option");
        // add option value
        option.value = classes[x];
        // add text of the option
        option.text = classes[x];
        // add the option to the previously found select box element
        currOptions.add(option);
      }
    }
  }
}
