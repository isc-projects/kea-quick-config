function morePools(sid,pid) {
  // sid = subnet id (required)
  // pid = pool id (required)
  if (!sid || !pid) {
    return;
  }
  // these may not be an integer depending how it was retreived
  sid = Number(sid);
  pid = Number(pid);
  // There will be a div container with a predictable id (poolContainer1)
  // based on the incoming sid
  // create div container id - ex: poolContainer1
  let containerID = 'poolContainer'+sid;
  // check that div container element exists or do none of the below
  let container = document.getElementById(containerID);
  if (container) {
    // create pool id - ex: pool1-1 
    let poolID = 'pool'+sid+'-'+pid;
    // check to make sure that poolID doesn't already exist.  return if it does
    if (document.getElementById(poolID)) {
      return;
    }
    // create span id - ex: pool1-1Val
    let spanID = poolID+'Val';
    // create pool input field name - ex: pool1-1
    let poolName = poolID;
    // create poolIDclass ID - ex: poolClientClass1-1
    let poolIDclass = 'poolClientClass'+sid+'-'+pid;
    // create sidclass - ex: poolClientClass1-1Val
    let sidclass = poolIDclass+'Val';
    // create next pool id for use in calling this function again
    let npid = pid+1;
    // create morePools span id
    let mpspid = 'NewPool'+sid;

    // create div element to be appened
    let newDR = document.createElement("div");
    //newDR.className = 'divrow';
    // create string of html with above created values
    const lines = [];
    lines.push('<div class=divrow>');
    lines.push('  <div class=divlabel>');
    lines.push('    <label for='+poolID+' class=labelpad>Pool:</label>');
    lines.push('  </div>');
    lines.push('  <div class=divfield>');
    lines.push('    <span id='+spanID+' class=formInput>');
    lines.push('      <input type=text id='+poolID+' name='+poolName+' value=\'\' placeholder=\'e.g. 192.0.2.2 - 192.0.2.254\'>');
    lines.push('    </span>');
    lines.push('    <span id='+mpspid+'>');
    lines.push('      <a class=ANOSTYLE href=# onclick="morePools('+sid+','+npid+');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
    lines.push('     </span>');
    lines.push('  </div>');
    lines.push('</div>');
    // client-class field as part of pool: https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#pool-selection-with-client-class-reservations
    // get array of current classes
    const classes = GetConfiguredClasses();
    // add KNOWN / UNKNOWN to the array
    classes.push('KNOWN');
    classes.push('UNKNOWN');
    // Now add the select box for client-class
    lines.push('<!-- <div class=divrow>');
    lines.push('  <div class=divlabel>');
    lines.push('    <label for='+poolIDclass+' class=labelpad>Pool client-class:</label>');
    lines.push('  </div>');
    lines.push('  <div class=divfield>');
    lines.push('    <span id='+sidclass+' class=formInput>');
    lines.push('      <select id='+poolIDclass+' name='+poolIDclass+'>&nbsp;');
    lines.push('        <option value=>Select Class Restriction</option>');
    for (i = 0; i < classes.length; i++) {
      lines.push('        <option value='+classes[i]+'>'+classes[i]+'</option>');
    }
    lines.push('      </select>');
    lines.push('    </span>');
    lines.push('  </div>');
    lines.push('</div>-->');
    let txt = '';
    for (i = 0; i < lines.length; i++) {
      txt = txt.concat(lines[i]);
    }
    // add inner html to div element
    newDR.innerHTML = txt;
    // need to delete the <img> element and link from the previous one if exist
    const element = document.getElementById(mpspid);
    if (element) {
      element.remove();
    }
    // append div element div container
    container.appendChild(newDR);
  }
  // thats it
}
