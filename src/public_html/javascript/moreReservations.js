function moreReservations(n) {
  // This function takes an integer as input that gives a unique id number
  // that is used to create the various variable content in the form field content
  // that is added to the ReservationContainer so that more than one host reservation
  // may be added at the global level (and later at other levels).  At the global level
  // IP address assignments are not supported.

  // n is required
  if (!n) {
    return;
  }

  // n may not be an integer depending how it was retreived
  n = Number(n);

  // Create the variable content that will be used in the form fields
  // fname-hwaddr
  let fnamehwaddr = 'reservation-hwaddr'+n;

  // if the element exists already, lets bail out
  if (document.getElementById(fnamehwaddr)) {
    return;
  }

  // sid-hwaddr
  let sidhwaddr = fnamehwaddr+'Val';

  // sid
  let sid = 'more-reservation-id';

  // fname-hostname
  let fnamehostname = 'reservation-hostname'+n;

  // sid-hostname
  let sidhostname = fnamehostname+'Val';

  // fnameclasses
  let fnameclasses = 'reservation-client-classes'+n;

  // sidclasses
  let sidclasses = fnameclasses+'Val';

  // next
  let next = n+1;

  // create optionContainer1 id
  let OCID = 'ReservationoptionContainer'+n;

  // Use the above IDs in creating the output HTML that will be added to
  // the container: ReservationContainer
  // create the div object
  let newDIV = document.createElement("div");
  // create array of lines of html
  const lines = [];

  // Initially, will need hw-addr field, and hostname field.
  // hw-addr
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fnamehwaddr+' class=labelpad>hw-addr:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sidhwaddr+' class=formInput>');
  lines.push('      <input type=text id='+fnamehwaddr+' name='+fnamehwaddr+' value=\'\' placeholder=\'e.g., 00:00:00:11:11:11\'>&nbsp;');
  lines.push('    </span>');
  lines.push('    <span id='+sid+'>');
  lines.push('      <a href=# class=ANOSTYLE onclick="moreReservations('+next+');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');

  // hostname
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fnamehostname+' class=labelpad>Hostname:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sidhostname+' class=formInput>');
  lines.push('      <input type=text id='+fnamehostname+' name='+fnamehostname+' value=\'\' placeholder=\'e.g., computer1\'>&nbsp;');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');

  // Reserving client classes https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#reserving-client-classes-in-dhcpv4
  // step 1, get a list of client classes to use as options to the select box (separate function below)
  const classes = GetConfiguredClasses();
  // step 2, use this list to add options to a select box to be shown
  // step 3, select box to allow multiple options to be selected
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fnameclasses+' class=labelpad>client-classes:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sidclasses+' class=formInput>');
  lines.push('      <select id='+fnameclasses+' name='+fnameclasses+' size=3 multiple>&nbsp;');
  for (i = 0; i < classes.length; i++) {
    lines.push('        <option value='+classes[i]+'>'+classes[i]+'</option>');
  }
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // The add option select box
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for=toAddReservationOption'+n+' class=labelpad>Add Reservation Option:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id=toAddReservationOption'+n+'Val class=formInput>');
  lines.push('      <select id=toAddReservationOption'+n+' name=toAddReservationOption'+n+' onfocus="GlobalOptionsSelectBoxPopulate(this.id,\'Reservation'+n+'\');">');
  lines.push('        <option value=>Select...</option>');
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('    <span id=toAddReservationOption'+n+'IMG>');
  lines.push('      <a href=# class=ANOSTYLE onclick="moreGlobalOptions(false,\''+OCID+'\',\'toAddReservationOption'+n+'\',\'Reservation'+n+'\');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // the container div that will house the options added to this client class :flushed:
  lines.push('<div id='+OCID+'>');
  lines.push('</div>');


  // create txt record from array
  // add a break between reservations which may have dynamic content (such as one or more option fields)
  let txt = '';
  if (n > 1) {
    txt = txt.concat('<div><hr></div>');
  }
  for (i = 0; i < lines.length; i++) {
    txt = txt.concat(lines[i]);
  }
  newDIV.innerHTML = txt;
  // need to delete the <img> element and link from the previous one if n is > 1
  const element = document.getElementById(sid);
  if (element) {
    element.remove();
  }
  // now append the new input field
  document.getElementById("ReservationContainer").appendChild(newDIV);

}

