function moreClientClasses(n) {
  // such function takes as input some integer which should be used
  // to instantiate a new class form field set which is named the same
  // except for the incoming integer (e.g., let let fname = 'clientClass'+n)
  // incoming 'n' is required and is the number of shared network
  if (!n) {
    return;
  }
  // n may not be an integer depending how it was retreived
  n = Number(n);
  // creates form field id of 'clientClass'+n
  //let fid = 'clientClass-name'+n;
  // check to make sure that fid doesn't already exist.  return if it does
  // creates span id of 'clientClass'+n+'Val'
  let sid = 'clientClass-name'+n+'Val';
  let sTestSelectId = 'clientClass-tsel'+n+'Val';
  let sTestInputId  = 'clientClass-tinp'+n+'Val';
  // creates form field name of 'clientClass'+n
  // make sure all these form names are 16 characters + n
  let fname       = 'clientClass-name'+n;
  let fTestSelect = 'clientClass-tsel'+n;
  let fTestInput  = 'clientClass-tinp'+n;
  if (document.getElementById(fname)) {
    return;
  }
  // creates placeholder text of 'e.g. SomeClass'+n
  let placeholder = 'e.g. SomeClass'+n;
  let placeholderTestInput = 'See ARM for details';
  // creates next value to be set (for further calls to this function)
  let next = n+1;
  // create optionContainer1 id
  let OCID = 'ClientClassoptionContainer'+n;

  // Use the above IDs in creating the output HTML that will be added to
  // the container: ClientClassificationContainer
  // create the div object
  let newDIV = document.createElement("div");
  // create array of lines of html
  const lines = [];
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fname+' class=labelpad>Client Class:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sid+' class=formInput>');
  lines.push('      <input type=text id='+fname+' name='+fname+' value=\'\' placeholder=\''+placeholder+'\'>&nbsp;');
  lines.push('    </span>');
  lines.push('    <span id=NewclientClass>');
  lines.push('      <a href=# class=ANOSTYLE onclick="moreClientClasses('+next+');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // The test line is defined below.  This is a very advanced feature in Kea where things like this are possible:
  // "test": "ifelse(substring(option[61].hex,4,3) == 'foo', substring(option[12].hex,0,12), '')",
  // To avoid the users of this kea-quick-config learning the complexities of the test line, we will try to have
  // only simple things possible.  A select box will be shown with simple tests like:
  // hexstring(pkt4.mac, ':')  -- where appropriate value would be 00:01:02:03:04:05
  // option[82].option[1].hex  -- where appropriate value would be fluffy bunnies (I think it decodes to ascii - test this)
  // option[82].option[2].hex  -- where appropriate value would be 00:01:02:03:04:05 (these usually have a mac address - test this)
  // member()                  -- where appropriate value would be KNOWN or some previously defined class name (need to detect these)
  // not member()              -- where appropriate value would be KNOWN or some previously defined class name (need to detect these)
  // The values will be in a separate field, of course.
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fTestSelect+' class=labelpad>Test Expression:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sTestSelectId+' class=formInput>');
  //lines.push('      <input type=text id='+fTestSelect+' name='+fTestSelect+' value=\'\' placeholder=\'NONE: SELECT BOX\'>&nbsp;');
  lines.push('      <select id='+fTestSelect+' name='+fTestSelect+' onchange="populateMccPlaceholder(this,'+n+')">');
  lines.push('        <option value=>Expression...</option>');
  lines.push('        <option value="hexstring(pkt4.mac, \':\')">hexstring(pkt4.mac, \':\')</option>');
  lines.push('        <option value="option[82].option[1].hex">option[82].option[1].hex </option>');
  lines.push('        <option value="option[82].option[2].hex">option[82].option[2].hex</option>');
  lines.push('        <option value="member()">member()</option>');
  lines.push('        <option value="not member()">not member()</option>');
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // Value to test for goes here:
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for='+fTestInput+' class=labelpad>Test Value:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id='+sTestInputId+' class=formInput>');
  lines.push('      <input type=text id='+fTestInput+' name='+fTestInput+' value=\'\' placeholder=\''+placeholderTestInput+'\'>&nbsp;');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // The add option select box
  lines.push('<div class=divrow>');
  lines.push('  <div class=divlabel>');
  lines.push('    <label for=toAddClassOption'+n+' class=labelpad>Add Class Option:</label>');
  lines.push('  </div>');
  lines.push('  <div class=divfield>');
  lines.push('    <span id=toAddClassOption'+n+'Val class=formInput>');
  lines.push('      <select id=toAddClassOption'+n+' name=toAddClassOption'+n+' onfocus="GlobalOptionsSelectBoxPopulate(this.id,\'ClientClass'+n+'\');">');
  lines.push('        <option value=>Select...</option>');
  lines.push('      </select>');
  lines.push('    </span>');
  lines.push('    <span id=toAddClassOption'+n+'IMG>');
  lines.push('      <a href=# class=ANOSTYLE onclick="moreGlobalOptions(false,\''+OCID+'\',\'toAddClassOption'+n+'\',\'ClientClass'+n+'\');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>');
  lines.push('    </span>');
  lines.push('  </div>');
  lines.push('</div>');
  // the container div that will house the options added to this client class :flushed:
  lines.push('<div id='+OCID+'>');
  lines.push('</div>');

  // create txt record from array
  let txt = '';
  if (n > 1) {
    txt = txt.concat('<div><hr></div>');
  }
  for (i = 0; i < lines.length; i++) {
    txt = txt.concat(lines[i]);
  }
  newDIV.innerHTML = txt;
  // need to delete the <img> element and link from the previous one if n is > 1
  const element = document.getElementById("NewclientClass");
  if (element) {
    element.remove();
  }
  // now append the new input field
  document.getElementById("ClientClassificationContainer").appendChild(newDIV);
  // now, initially toggle the Placeholder
  // need to get the select box object to pass
  let obj = document.getElementById(fTestSelect);
  populateMccPlaceholder(obj,n);
}

function populateMccPlaceholder(selectObject,n) {
  let testExpression = selectObject.value;
  let InputBoxID = 'clientClass-tinp'+n;
  let placeholder = '';
  document.getElementById(InputBoxID).disabled = false;
  if (testExpression == "hexstring(pkt4.mac, \':\')") {
    placeholder = 'e.g., 00:00:00:11:11:11';
  } else if (testExpression == "option[82].option[1].hex") {
    placeholder = 'e.g., VLAN12';
  } else if (testExpression == "option[82].option[2].hex") {
    placeholder = 'e.g., 22:22:22:33:33:33';
  } else if (testExpression == "member()") {
    placeholder = 'e.g., KNOWN';
  } else if (testExpression == "not member()") {
    placeholder = 'e.g., Some Previously Defined Class Name';
  } else if (testExpression == "") {
    // blank option should say something like select a test expression
    // maybe even disable the input box!
    placeholder = 'Select Test Expression';
    document.getElementById(InputBoxID).disabled = true;
  } else {
    console.log("Unknown Test Expression "+testExpression);
  }
  // and finally, set the chosen placeholder
  document.getElementById(InputBoxID).placeholder = placeholder;
}
