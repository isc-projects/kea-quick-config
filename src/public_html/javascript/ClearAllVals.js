// start over for the user
function ClearAllVals(str) {
  // str contains nothing (or unknown value) or CANCELED or CONFIRMED
  if (str == 'CANCELED') {
    // if it is CANCELED, we will hide the confirmation dialog and do nothing else
    document.getElementById('PreventClicks').style.display = 'none';
    document.getElementById('StartOverConfirm').style.display = 'none';
  } else if (str == 'CONFIRMED') {
    // if it is CONFIRMED, then we will hide the confirmation dialog and perform the regular steps
    document.getElementById('PreventClicks').style.display = 'none';
    document.getElementById('StartOverConfirm').style.display = 'none';
    // make array of all keys stored in session storage in the browser
    let Keys = Object.keys(sessionStorage);
    for (let i = 0; i < Keys.length; i++) {
      // Now delete all these keys
      DelItem(Keys[i]);
    }
    // fixup for Firefox that does not clear the data from the forms for some reason
    // the below does not hurt Safari or Chrome based browsers
    let forms = document.getElementsByTagName('form');
    for (let i=0; i < forms.length; i++) {
      let Id=forms[i].id;
      document.getElementById(Id).reset();
    }
    // Now get all the stored form data which will set the form data to not filled out
    //GetStoredFormData();
    // Now return to the start page
    //ShowDiv('ConfTypeChooser');
    // actually we need to reload the page
    location.reload();
  } else {
    // if it is empty or contains an unknown value, then we will display the confirmation dialog and do nothing else
    document.getElementById('PreventClicks').style.display = 'block';
    document.getElementById('StartOverConfirm').style.display = 'block';
  }
}
