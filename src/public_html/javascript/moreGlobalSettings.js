/*
  This function adds a global setting to the GlobalSettingsContainer
  It finds which setting to add by what is selected in toAddGlobalSetting
  It checks if the setting is already added to the form and doesn't add if so
  It gets a data set of these settings from globalSettingsList()
  It then adds the form field from the HTML stored in the list
  Finally, it sets the default value as stored in the list
*/
function moreGlobalSettings(iNAME) {
  // what setting are we to add?
  // get the currently selected options value which will match the name
  // of some setting from list
  let optionSelectedValue = '';
  if (!iNAME) {
    let elem = document.getElementById("toAddGlobalSetting");
    let eSi = elem.options.selectedIndex;
    optionSelectedValue = elem.options[eSi].value;
  } else {
    optionSelectedValue = iNAME;
  }
  // just return if this is blank
  if (!optionSelectedValue) {
    return false;
  }
  // was this setting already added?
  if (document.getElementById(optionSelectedValue)) {
    // Ignore if so
  } else {
    // add the form field to the form if not
    // get the list of Global Settings
    const list = globalSettingsList();
    // Get the HTML from the list
    for (let i = 0; i < list.length; i++) {
      if (list[i].Name == optionSelectedValue) {
        // add the HTML to the select box
        let txt = '<div class=divrow>'+list[i].HTML+'</div>';
        let newDIV = document.createElement("div");
        newDIV.innerHTML = txt;
        document.getElementById('GlobalSettingsContainer').appendChild(newDIV);

        // add the URL to the ARM as well
        txt = '<div class=divrow><a href="'+list[i].ARMurl+'" target=_new>'+list[i].ARMtitle+'</a></div>';
        newDIV = document.createElement("div");
        newDIV.innerHTML = txt;
        document.getElementById('GlobalSettingsContainer').appendChild(newDIV);

        // add separation of some kind
        txt = '<div class=divrow>---</div>';
        newDIV = document.createElement("div");
        newDIV.innerHTML = txt;
        document.getElementById('GlobalSettingsContainer').appendChild(newDIV);

        // now set the default value as defined in the list
        let defaultValue = list[i].Default;
        // Get the form field element
        // if there are no checkbox or radio (and there won't be so far)
        // can just set the value which will change what is selected
        document.getElementById(optionSelectedValue).value = defaultValue;
        // Now set the Global Settings box back to a blank value to prevent
        // saving of a value in the box
        document.getElementById('toAddGlobalSetting').value = '';
      }
    }
  }
}
