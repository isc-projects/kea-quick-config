// This function will be executed on page load as well as
// after ProcessVals() on next button click (and on back button click)
function GetStoredFormData() {
  // make array of all keys stored in session storage in the browser
  let Keys = Object.keys(sessionStorage);
  // we can sort them by the original input field name
  Keys.sort();
  // Some of these need to be added in order (pools following subnets, for example)
  // so multiple arrays are required that will be processed later
  // subnets first where a subnet div will be created for the id
  const subnets = [];
  // then shared network select box selected value (no creation)
  const subnetSNselectBoxes = [];
  // then router key value (no creation)
  const subnetRouters = [];
  // then pools
  const subnetPools = [];
  for (let i = 0; i < Keys.length; i++) {
    // get the value stored for the key
    let value = ReadItem(Keys[i]);
    if (Keys[i].substr(0,13) == 'sharedNetwork') {
      // if is a shared netowrk, then we need to probably add structure for it
      let npart = Keys[i].substr(13);
      moreSharedNetworks(npart);
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,4) == 'pool') {
      subnetPools.push(Keys[i]+'_|_'+value);
    } else if (Keys[i].substr(0,25) == 'subnetSharedNetworkSelect') {
      subnetSNselectBoxes.push(Keys[i]+'_|_'+value);
   } else if (Keys[i].substr(0,6) == 'subnet') {
      subnets.push(Keys[i]+'_|_'+value);
      // create the (possibly) additional subnet boxen
      let npart = Keys[i].substr(6);
      moreSubnets(npart);
    } else if (Keys[i].substr(0,6) == 'router') {
      subnetRouters.push(Keys[i]+'_|_'+value);
    } else if (Keys[i].substr(0,18) == 'GlobalSettingsList') {
      // Need to create the element
      moreGlobalSettings(Keys[i]);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,17) == 'GlobalOptionsList') {
      // Need to create the element
      moreGlobalOptions(Keys[i]);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,11) == 'ClientClass' && Keys[i].includes("_|NGO|_")) {
      const parts = Keys[i].split("_|NGO|_");
      let PREFIX = parts[0];
      let OCID = PREFIX.substr(0,11) + 'optionContainer' + PREFIX.substr(11);
      // Need to create the element
      moreGlobalOptions(Keys[i],OCID,false,PREFIX);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,11) == 'Reservation' && Keys[i].includes("_|NGO|_")) {
      const parts = Keys[i].split("_|NGO|_");
      let PREFIX = parts[0];
      let OCID = PREFIX.substr(0,11) + 'optionContainer' + PREFIX.substr(11);
      // Need to create the element
      moreGlobalOptions(Keys[i],OCID,false,PREFIX);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,6) == 'Subnet' && Keys[i].includes("_|NGO|_")) {
      const parts = Keys[i].split("_|NGO|_");
      let PREFIX = parts[0];
      let OCID = PREFIX.substr(0,6) + 'optionContainer' + PREFIX.substr(6);
      // Need to create the element
      moreGlobalOptions(Keys[i],OCID,false,PREFIX);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
    } else if (Keys[i].substr(0,11) == 'clientClass') {
      // create the element but only once when it is '-name'
      // get ID
      mCCn=Keys[i].substr(16);
      moreClientClasses(mCCn);
      // Need to set the element's value using GetStoredFormDataSV
      GetStoredFormDataSV(Keys[i],value);
      // In this particular instance, the form input box will be disabled
      // we must enable since it now has a value and test expression chosen
      let mccSelID = 'clientClass-tsel'+mCCn;
      let selectMccObject = document.getElementById(mccSelID);
      populateMccPlaceholder(selectMccObject,mCCn);
    } else if (Keys[i].substr(0,11) == 'reservation') {
      // get the id to be sent to the function`
      let resID = 1;
      if (Keys[i].substr(0,18) == 'reservation-hwaddr') {
        resID = Keys[i].substr(18);
      } else if (Keys[i].substr(0,20) == 'reservation-hostname') {
        resID = Keys[i].substr(20);
      }
      // Create the element (moreReservations() will bail out if the form elements already exist for this resID)
      moreReservations(resID);
      // set the value
      GetStoredFormDataSV(Keys[i],value);
    } else {
      GetStoredFormDataSV(Keys[i],value);
    }
  }
  // now lets run through each array in the same manner as before
  // first subnets
  for (i = 0; i < subnets.length; i++) {
    const parts = subnets[i].split("_|_");
    let key = parts[0];
    let value = parts[1];
    GetStoredFormDataSV(key,value);
  }
  // then populate shared network select boxes
  for (i = 0; i < subnetSNselectBoxes.length; i++) {
    const parts = subnetSNselectBoxes[i].split("_|_");
    let key = parts[0];
    let value = parts[1];
    GetStoredFormDataSV(key,value);
  }
  // then populate routers
  for (i = 0; i < subnetRouters.length; i++) {
    const parts = subnetRouters[i].split("_|_");
    let key = parts[0];
    let value = parts[1];
    GetStoredFormDataSV(key,value);
  }
  // then add and populate pools
  for (i = 0; i < subnetPools.length; i++) {
    const parts = subnetPools[i].split("_|_");
    let key = parts[0];
    let value = parts[1];
    // add structure for pool
    let str = key.substr(4);
    const ids = str.split("-");
    let sid = ids[0];
    let pid = ids[1];
    morePools(sid,pid);
    GetStoredFormDataSV(key,value);
  }
}

function GetStoredFormDataSV(name,value) {
  let elems = document.getElementsByName(name);
  for (let n = 0; n < elems.length; n++) {
    let elem = elems[n];
    if (elem.type=="radio" || elem.type=="checkbox") {
      // radio buttons and checkboxes require special handling as we have to detect what was checked
      // since they are both the same name
      // or in the case of checkboxen, they also use the "checked" element
      if (elem.value==value) {
        // once we find the matching value, set checked for this element to true!
        elem.checked = true;
      }
    } else if (elem.type=="select-one") {
      // select box can just have value set
      elem.value=value;
    } else if (elem.type=="select-multiple") {
      // This type requires splitting on _|_ which is the separator I used to get what was originally selected
      const parts = value.split("_|_");
      // iterate through all of the options of this multi-select box
      for (let i = 0; i < elem.options.length; i++) {
        // there is no in_array() function like there is in php so a simple for to iterate the array is in order
        for (let x = 0; x < parts.length; x++) {
          // if we find it in the current array part matches the current option, set selected true
          if (parts[x]==elem.options[i].value) {
            elem.options[i].selected = true;
          }
        }
      }
    } else if (elem.type=="text") {
      // text can just set the value
      elem.value=value;
    } else if (elem.type=="hidden") {
      // hidden type can just set the value
      elem.value=value;
    } else {
      // log any element types that we haven't handled yet so that we know we need to
      // update this function!
      console.log(elem.type.concat(" type is undefined in GetStoredFormData() function"));
    }
  }
}
