/*
  This function gets a list of settings from globalSettingsList()
  Then it populates the Add Global select box from GlobalSettings.html
    with a list of possible settings to add that have not ALREADY been
    added.
  The list is only populated with settings that are not already added.
*/
function GlobalSettingsSelectBoxPopulate() {
  // get data from function
  const list = globalSettingsList();
  // The actual element to which we will add
  let elem = document.getElementById("toAddGlobalSetting");
  // empty the setting dropdown first (except the first option)
  let n = elem.length;
  for (let i = 0; i < n; i++) {
    // always remove element 1 as the element postion
    // resets somehow but doesn't from the perspective of .add below
    // more research would be required to figure this out, but this
    // works currently so I am choosing to save time and not perform
    // the research
    elem.remove(1);
  }
  // The data is an array of objects with details about each setting
  //let check = false;
  for (let i = 0; i < list.length; i++) {
    // in this function, we only need the name
    let settingName = list[i].Name;
    // we don't want to add it if the setting has already been
    // added to the form so we check for that here
    let check = false;
    if (document.getElementById(settingName)) {
      check = true;
    }
    if (!check) {
      // Not already added, lets add to the toAddGlobalSetting id
      // create option
      let option = document.createElement("option");
      option.value = settingName;
      let settingTEXT = settingName.substr(19);
      option.text = settingTEXT;
      // add option to select element at the specific position
      // This is because the index of the element doesn't reset
      // when the existing elements are removed and yet it does
      // in some sense as elem.remove(1) was necessary above.
      let pos = i+1;
      elem.add(option, elem[pos]);
      //console.log("I added option "+settingName+" at position "+pos);
    }
  }
}
