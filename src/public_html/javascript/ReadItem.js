// Read and return a value from browser session storage based on supplied key
function ReadItem(Key1) {
  // get the value out of session storage
  let Value1=sessionStorage.getItem(Key1);
  if (Value1) {
    // if there be something in the value, return said thing
    return Value1;
  } else if (Value1==="") {
    // value was empty string
    return Value1;
  } else {
    // else return false
    return false;
  }
}
