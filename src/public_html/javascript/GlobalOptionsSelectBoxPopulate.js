/*
  This function gets a list of Options from globalOptionsList()
  Then it populates the Add Global select box from GlobalOptions.html
    with a list of possible Options to add that have not ALREADY been
    added.
  The list is only populated with Options that are not already added.
*/
function GlobalOptionsSelectBoxPopulate(id,FormT) {
  // get data from function
  const list = globalOptionsList();
  // did we get an id? it is global if not:
  if (id) {
    // do nothing
  } else {
    id = "toAddGlobalOption";
  }
  // add separator to Form variable to distinguish from global
  // this will be needed below when checking and also when submitting
  // validation will need to know which options same for config output
  let FormS = '';
  if (FormT) {
    FormS = FormT + '_|NGO|_';
  }
  // The actual element to which we will add
  let elem = document.getElementById(id);
  // empty the Option dropdown first (except the first option)
  let n = elem.length;
  for (let i = 0; i < n; i++) {
    // always remove element 1 as the element postion
    // resets somehow but doesn't from the perspective of .add below
    // more research would be required to figure this out, but this
    // works currently so I am choosing to save time and not perform
    // the research
    elem.remove(1);
  }
  // The data is an array of objects with details about each Option
  //let check = false;
  for (let i = 0; i < list.length; i++) {
    // in this function, we only need the name
    let optionName = list[i].Name;
    // we don't want to add it if the Option has already been
    // added to the form so we check for that here
    let check = false;
    let CheckID = FormS + optionName;
    if (document.getElementById(CheckID)) {
      check = true;
    }
    if (!check) {
      // Not already added, lets add to the toAddGlobalOption id
      // create option
      let option = document.createElement("option");
      let optionValue = optionName;
      if (FormT) {
        optionValue = FormT + '_|NGO|_' + optionName;
      }
      option.value = optionValue;
      let optionTEXT = optionName.substr(18);
      option.text = optionTEXT;
      // add option to select element at the specific position
      // This is because the index of the element doesn't reset
      // when the existing elements are removed and yet it does
      // in some sense as elem.remove(1) was necessary above.
      let pos = i+1;
      elem.add(option, elem[pos]);
      //console.log("I added option "+optionName+" at position "+pos);
    }
  }
}
