// Show the chosen div and hide the rest
function ShowDiv(Sid) {
  // get the element (div) by the submitted id
  let elem = document.getElementById(Sid);
  // toggle on the display of the div
  elem.style.display = 'block';
  //elem.style.height = '500px';
  elem.style.height = '80%';
  elem.style.minHeight = '500px';
  // get all the divs
  let divs = document.getElementsByTagName("div");
  // create list of div names to toggle
  const Tdivs = ["theEnd",
                 "LoggerSetup",
                 "SubnetSetup",
                 "SharedNetworkSetup",
                 "LeaseDatabaseSetup",
                 "InterfaceSetup",
                 "ControlSocketSetup",
                 "ConfTypeChooser",
                 "GlobalSettings",
                 "GlobalOptions",
                 "ClientClassificationSetup",
                 "ReservationSetup",
                 "HooksSetup"
                ];
  // iterate through the list of divs
  for(let i = 0; i < divs.length; i++){
    // find the id of the current div
    let id = divs[i].id;
    // compare to the submitted id
    if (id != Sid) {
      // toggle off the display of the div if the id does not match the submitted id
      // only toggle the div if it is in Tdivs array
      if (Tdivs.includes(id)) {
        divs[i].style.display = 'none';
        divs[i].style.height = '0px';
        divs[i].style.minHeight = '0px';
      }
    }
  }
}
