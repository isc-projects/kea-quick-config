// Generic function for each page to execute that controls validation of each form
// This replaces the old ProcessVals() that submitted to PHP backend
// The new validation will occur in Javascript
function ProcessVals2(formID,CurrDiv) {
  // now lets blank the error message i think
  document.getElementById("error").innerHTML = "";
  // validation triplet starts as false
  let returnStr = '';
  // now validate based on formID submitted
  if (formID == 'ConfTypeChooserForm') {
    returnStr = ValidateConfType(formID);
  } else {
    returnStr = '_|__|_UNKNOWN FORM';
  }
  // set the results from the above return strings
  const returnElems = returnStr.split('_|_');
  let resCode = returnElems[0];
  let errFld = returnElems[1];
  let errMesg = returnElems[2];
  if (resCode=="FAIL") {
    // note the error
    document.getElementById("error").innerHTML = '<p class=error>'+errMesg+'</p>';
    // highlight the formfield that failed validation
    document.getElementById(errFld.concat("Val")).style.border = '1px solid red';
    ShowDiv(CurrDiv);
    return false;
  } else if (resCode== "PASS") {
    StoreValidatedData(formID);
    GetStoredFormData();
  } else {
    if (!errMesg) {
      errMesg = "UNKNOWN RESULT CODE";
    }
    document.getElementById("error").innerHTML = "<p class=error>"+errMesg+"</p>";
    ShowDiv(CurrDiv);
    return false;
  }
}
