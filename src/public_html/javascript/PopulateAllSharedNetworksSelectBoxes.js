// Populate all shared network select boxes when executed
function PopulateAllSharedNetworksSelectBoxes() {
  // lets get all of the select boxes that need populating by classname
  const formElems = document.getElementsByClassName("subnetSharedNetworkSelect");
  if (formElems) {
    for (let i=0; i < formElems.length; i++) {
      // Now lets proceed through the list of elements and pass the ids to the 
      // populateSharedNetworks function
      populateSharedNetworks(formElems[i].id);
    }
  }
}
