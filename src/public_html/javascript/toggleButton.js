function toggleButton(id, text) {
  // detect current status of button
  let status = document.getElementById(id).disabled;
  // flip button to opposite status
  if (status) {
    document.getElementById(id).disabled = false;
  } else {
    document.getElementById(id).disabled = true;
  }
  // apply text to button, if supplied
  if (text) {
    document.getElementById(id).innerHTML = text;
  }
}
