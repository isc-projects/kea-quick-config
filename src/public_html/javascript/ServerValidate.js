// submit the identified form for validation
function ServerValidate(formID) {
  // initialize values
  let fail = false;
  let someErr = '';
  // The post data will be in here
  let str = '';
  // what names we have already added
  const USED = [];
  // first get the contents of the form into an array
  let inputs = document.getElementById(formID).elements;
  for(let i = 0; i < inputs.length; i++){
    // get the name
    let elemName = inputs[i].name;
    // get the value below depending on type of input
    let elemValue;
    if (inputs[i].type=="radio" || inputs[i].type=="checkbox") {
      if (inputs[i].checked) {
        elemValue = inputs[i].value;
      } else {
      }
    } else if (inputs[i].type=="select-one") {
      elemValue = inputs[i].value;
    } else if (inputs[i].type=="select-multiple") {
      // this type requires iterating the options that were present in the select element and adding those
      // that were selected to the elemValue
      // I chose to use an elaborate separator ( _|_ ) for no ambiguity splitting later
      let txt = '';
      for (let x = 0; x < inputs[i].options.length; x++) {
        if (inputs[i].options[x].selected) {
          let c=inputs[i].options[x].value;
          if (txt) {
            txt = txt.concat('_|_');
          }
          txt = txt.concat(c);
        }
      }
      elemValue = txt;
    } else if (inputs[i].type=="text" || inputs[i].type=="hidden") {
      elemValue = inputs[i].value;
    } else {
      // uh oh ... we used an unknown type
      console.log(inputs[i].type.concat(" type is undefined in ServerValidate(formID) function"));
    }
    if (USED.includes(elemName)) {
      // we already attached this var
    } else {
      if (elemValue) {
        if (elemValue.includes("&")) {
          // we cannot get reliable answers if the separator is included
          // javascript encodeURI ignores &
          fail = true;
          someErr = 'Cannot validate when a field contains &';
        }
        if (str) {
          str = str.concat("&");
        }
        str = str.concat(encodeURI(elemName),"=");
        str = str.concat(encodeURI(elemValue));
        USED.push(elemName);
      }
    }
  }
  // We should now have str ... lets post!
  // we will set a return value for the end
  let returnVal = false;
  let txt = ServerComm(str,"?validate=true&form="+formID,'POST');
  if (txt) {
    const result = txt.split("_|_");
    let resCode = result[0];
    let errFld= result[1];
    let errMesg = result[2];
    // set all of the spans to default borderless with <something>.style.border = ''; I hope
    const elems = document.getElementsByClassName("formInput");
    for (let i = 0; i < elems.length; i++) {
      elems[i].style.border = '0px solid white';
    }
    if (resCode == "PASS") {
      // The validation succeeded!
      // blank the error div just in case
      document.getElementById("error").innerHTML = '';
      returnVal = true;
    } else if (resCode=="FAIL") {
      // note the error
      document.getElementById("error").innerHTML = '<p class=error>'+errMesg+'</p>';
      // highlight the formfield that failed validation
      document.getElementById(errFld.concat("Val")).style.border = '1px solid red';
    } else {
      if (txt) {
        console.log(txt.concat(" <- result UNKNOWN in ServerValidate()"));
      } else {
        console.log("No return available");
      }
    }
  } else {
    console.log("communication error");
  }
  return returnVal;
}
