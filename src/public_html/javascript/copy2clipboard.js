function copy2clipboard(id,buttonid) {
  // submitted id MUST be a form field
  // buttonid must be the id of the button to be disabled and replaced with "copied!"
  // Find the element that contains the text to be copied
  let formElem = document.getElementById(id);

  let text = formElem.value;
  navigator.clipboard.writeText(text);
  // disable button and set to "copied!"
  toggleButton(buttonid, 'Copied!');
}
