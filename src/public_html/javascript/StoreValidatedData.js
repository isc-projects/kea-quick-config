// This function performs the data storage on behalf of ProcessVals2()
function StoreValidatedData(formID) {
  let inputs = document.getElementById(formID).elements;
  for(let i = 0; i < inputs.length; i++){
    let elemName = inputs[i].name;
    let elemValue = '';
    if (inputs[i].type=="radio" || inputs[i].type=="checkbox") {
      if (inputs[i].checked) {
        elemValue = inputs[i].value;
      }
    } else if (inputs[i].type=="select-one") {
      elemValue = inputs[i].value;
    } else if (inputs[i].type=="select-multiple") {
      // this type requires iterating the options that were present in the select element and adding those 
      // that were selected to the elemValue
      // I chose to use an elaborate separator ( _|_ ) for no ambiguity splitting later
      let txt = '';
      for (let x = 0; x < inputs[i].options.length; x++) {
        if (inputs[i].options[x].selected) {
          let c=inputs[i].options[x].value;
          if (txt) {
            txt = txt.concat('_|_');
          }
          txt = txt.concat(c);
        }
      }
      elemValue = txt;
    } else if (inputs[i].type=="text" || inputs[i].type=="hidden") {
      elemValue = inputs[i].value;
    } else {
      console.log(inputs[i].type.concat(" type is undefined in ProcessVals(formID) function"));
    }
    if (elemName && elemValue) {
      WriteItem(elemName,elemValue);
    } else if (elemName) {
      if (inputs[i].type=="radio") {
      } else {
        // DelItem(elemName);
        WriteItem(elemName,elemValue);
      }
    }
  }
}
