// remove item from storage
function DelItem(Key1) {
  // This is vary simple
  // We just want to remove the stored item from session storage
  try {
    sessionStorage.removeItem(Key1);
  }
  catch(err) {
    // if there was an error message, lets log it and return false
    console.log(err.message);
    return false;
  }
  // else return true for successful session storage of data in the web browser
  return true;
}
