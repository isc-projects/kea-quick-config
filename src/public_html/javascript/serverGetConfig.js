function serverGetConfig(TAid,buttonID) {
  // this submits the final collected values
  // retrieves the configuration
  // adds it to the textarea of the submitted TAid

  // step one, get all previously collected data
  let Keys = Object.keys(sessionStorage);
  // then iterate through the list
  // adding the key value pairs to str
  let str = '';
  for (let i = 0; i < Keys.length; i++) {
    // get keyname
    let key = Keys[i];
    // retrieve value
    let value = ReadItem(Keys[i]);
    // I now have the key and value
    // add them to string to be sent in post
    if (str) {
      str = str.concat("&");
    }
    str = str.concat(encodeURI(key),"=");
    str = str.concat(encodeURI(value));
  }
  let response = ServerComm(str,'?SHOWCONF=true','POST');
  if (response) {
    document.getElementById(TAid).innerHTML = response;
    // disable the button after successful communication
    toggleButton(buttonID, false);
  } else {
    console.log("communication error");
  }
}
