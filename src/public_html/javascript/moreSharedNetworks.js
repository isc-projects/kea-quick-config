function moreSharedNetworks(n) {
  // incoming 'n' is required and is the number of shared network
  if (!n) {
    return;
  }
  // n may not be an integer depending how it was retreived
  n = Number(n);
  // creates form field id of 'sharedNetwork'+n
  let fid = 'sharedNetwork'+n;
  // check to make sure that fid doesn't already exist.  return if it does
  if (document.getElementById(fid)) {
    return;
  }
  // creates span id of 'sharedNetwork'+n+'Val'
  let sid = 'sharedNetwork'+n+'Val';
  // creates form field name of 'sharedNetwork'+n
  let fname = 'sharedNetwork'+n;
  // creates placeholder text of 'e.g. VLAN'+n
  let placeholder = 'e.g. VLAN'+n;
  // creates next value to be set (for further calls to this function)
  let next = n+1;

  // Now that the above has been created,
  // create a new element that will be appended
  // to the sharedNetworkContainer div using
  // the appendChild() method
  let newDR = document.createElement("div");
  newDR.className = 'divrow';
  let dl  = '  <div class=divlabel>';
  let ll  = '    <label for='+fid+' class=labelpad>Shared Network:</label>';
  let dlc = '  </div>';
  let df  = '  <div class=divfield>';
  let sn  = '    <span id='+sid+' class=formInput>';
  let it  = '      <input type=text id='+fid+' name='+fname+' value=\'\' placeholder=\''+placeholder+'\'>&nbsp;';
  let snc = '    </span>';
  let s2  = '    <span id=NewSharedNetwork>';
  let img = '      <a href=# class=ANOSTYLE onclick="moreSharedNetworks('+next+');"><img width=20 height=20 src=images/add-icon-png-2480.png border=0></a>';
  let s2c = '     </span>';
  let dfc = '  </div>';
  let txt = dl+ll+dlc+df+sn+it+snc+s2+img+s2c+dfc;
  newDR.innerHTML = txt;

  // need to delete the <img> element and link from the previous one if n is > 1
  const element = document.getElementById("NewSharedNetwork");
  if (element) {
    element.remove();
  }
  // now append the new input field
  document.getElementById("sharedNetworkContainer").appendChild(newDR);
}
