/*
  This particular function creates an array with details of the global Options.
  This array is returned to the caller and the array contents are used to show
  the Options as well as populate a list of possible Options.
  The contents of the array are objects that describe the Option.

  Each object needs to contain the Option name, the type of value, the default
  value, and the HTML used for the Option.
  Also, the object should contain indicators of levels it is allowed.
  These indicators are for possbile future use.
*/
function globalOptionsList(PREFIX) {
  // string of HTML
  let tHTML = '';
  let fid = '';
  let sid = '';
  let fNAME = '';
  // container array
  const x = [];
  let preamble = '';
  if (PREFIX) {
    preamble = PREFIX + '_|NGO|_';
  }

  // Global Options objects
  // domain-name-servers
  fName=preamble + 'GlobalOptionsList-domain-name-servers';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Domain Name servers:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., 192.0.2.42, 192.0.2.82">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const domainNameServers = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // domain-name
  fName=preamble + 'GlobalOptionsList-domain-name';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Domain Name:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., example.com">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const domainName = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // ntp-servers
  fName=preamble + 'GlobalOptionsList-ntp-servers';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>NTP servers:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., 192.0.2.12, 192.0.2.13">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const ntpServers = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // time-offset
  fName=preamble + 'GlobalOptionsList-time-offset';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Time Offset:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., -18000">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const timeOffset = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // time-servers
  fName=preamble + 'GlobalOptionsList-time-servers';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Time Servers:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., 192.0.2.24, 192.0.2.25">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const timeServers = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // static-routes
  fName=preamble + 'GlobalOptionsList-static-routes';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Static Routes:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., 192.0.2.26, 192.0.2.27">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const staticRoutes = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // tftp-server-name
  // (may want to set 'sname' configuration directive nearby as well see: https://gitlab.isc.org/isc-projects/kea-quick-config/-/issues/16#note_417078)
  fName=preamble + 'GlobalOptionsList-tftp-server-name';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>TFTP Server Name:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., tftp.example.org">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const tftpServerName = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // boot-file-name
  // (may want to set 'file' configuration directive nearby as well see: https://gitlab.isc.org/isc-projects/kea-quick-config/-/issues/16#note_417078)
  fName=preamble + 'GlobalOptionsList-boot-file-name';
  fid=fName;
  sid=fName+'Val';
  tHTML = '  <div class=divlabel>'+
         '    <label for='+fid+' class=labelpad><small>Boot File Name:</small></label>'+
         '  </div>'+
         '  <div class=divfield>'+
         '    <span id='+sid+' class=formInput>'+
         '      <input type=text id='+fid+' name='+fName+' placeholder="e.g., boot-file.img">'+
         '      </input>'+
         '    </span>'+
         '  </div>';
  const bootFileName = {
    Name:fName,
    Type:"string",
    Default:"",
    HTML:tHTML,
    ARMurl:'https://kea.readthedocs.io/en/kea-2.4.1/arm/dhcp4-srv.html#standard-dhcpv4-options',
    ARMtitle:'8.2.10. Standard DHCPv4 Options',
  };

  // add the objects defined above to the returned list in the desired order
   x[0] = domainNameServers;
   x[1] = domainName;
   x[2] = ntpServers;
   x[3] = timeOffset;
   x[4] = timeServers;
   x[5] = staticRoutes;
   x[6] = tftpServerName;
   x[7] = bootFileName;

  return x;
}

