// Write the supplied name / value pair to session storage in the browser
function WriteItem(Name, Value) {
  // banned write list where we don't want to write any of these
  const banned = [ 'toAddGlobalSetting' ];
  if (banned.includes(Name)) {
    return true;
  }
  // lets write this submitted name and value pair to session storage
  try {
    sessionStorage.setItem(Name, Value);
  }
  catch(err) {
    // if there was an error message, lets log it and return false
    console.log(err.message);
    return false;
  }
  // else return true for successful session storage of data in the web browser
  return true;
}
