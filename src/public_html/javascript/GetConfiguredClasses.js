// This function gets the currently configured classes in ClientClassification.html's ClientClassificationSetupForm
// This can be used by various things that need to know about the currently configured classes

function GetConfiguredClasses() {
  // array of class names
  const classes = [];

  // loop over ClientClassificationSetupForm fields
  //const formContent = document.getElementById("ClientClassificationSetupForm");
  //console.log(formContent);
  //console.log(formContent.length);
  const formContent = Object.keys(sessionStorage);
  formContent.sort();
  //console.log(formContent);
  //console.log(formContent.length);
  for (let i = 0; i < formContent.length; i++) {
    // we are only looking for class names
    if (formContent[i].substr(0,16) == 'clientClass-name') {
      // and only if they contain a value
      if (ReadItem(formContent[i])) {
        // get the value from the name
        let elemValue = ReadItem(formContent[i]);
        // now add this value to the array
        classes.push(elemValue);
      }
    }
  }
  // return the array
  return classes;
}
