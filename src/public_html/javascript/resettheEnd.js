function resettheEnd() {
  // Toggle the Get Configuration button if it is disabled
  if (document.getElementById('getConfiguration').disabled) {
    toggleButton('getConfiguration',false);
  }
  // Toggle the 'Copy' button and reset the text if disabled
  if (document.getElementById('copyKeaConf').disabled) {
    toggleButton('copyKeaConf','Copy');
  }
  // and clear out the configuration that might be in the box
  document.getElementById('ShowKeaConfiguration').innerHTML = '';
}
