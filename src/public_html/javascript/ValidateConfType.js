// Validate the choice of configuration type (dhcp4 or dhcp6)
function ValidateConfType(formID) {
  let resCode = '';
  let errFld = '';
  let errMesg = '';
  let elems=document.getElementById(formID).elements;
  if (elems[0].checked) { // dhcp4
    resCode = 'PASS';
  } else if (elems[1].checked) { // dhcp6
    resCode = 'FAIL';
    errFld= 'ConfType';
    errMesg = 'DHCPv6 is not yet supported';
  } else { // none
    resCode = 'FAIL';
    errFld= 'ConfType';
    errMesg = 'You must select a configuration type';
  }
  return(resCode+'_|_'+errFld+'_|_'+errMesg);
}
