# Kea Quick Config

## Description
This project is a web-based tool for generating a Kea configuration that can be copied and pasted into your Kea server.  This is done by gathering information from a step by step process.

## Installation
To use locally, all that is required is php installed on the system where it is to be used.  Clone the git repository.  From a terminal screen, enter the repsitory and public_html directory (inside the src directory).  Then execute: `php -S localhost:8000` Then, in a web browser on the same host, visit URL: http://localhost:8000  You can, of course, tell the built-in php web server to listen on an external address, but this is not recommended.  If you wish to host the project publicly, it is best to have a full fledged web server (Apache or similar) with php support.

Installation in Docker see: https://gitlab.isc.org/isc-projects/kea-quick-config/-/wikis/Running-in-Docker-image

## Releases
- Download the 0.1 source release [here](https://gitlab.isc.org/isc-projects/kea-quick-config/-/raw/Keq-Quick-Config-deployment/kea-quick-config-0.1.tar.gz)
- 0.2 has been released see [here](https://gitlab.isc.org/isc-projects/kea-quick-config/-/releases/0.2)
- 0.3 has been released see [here](https://gitlab.isc.org/isc-projects/kea-quick-config/-/releases/0.3)
- 0.4 has been released see [here](https://gitlab.isc.org/isc-projects/kea-quick-config/-/releases/0.4)

## Support
This software is unsupported / experimental.

## Roadmap
Note that this is meant to be a simple GUI that creates a configuration that should be considered a starting point rather than a complete configuration.  Currently, the project is not complete but work continues though more slowly than in the past. 
Current Milestones: 
- `0.5` is planned to remove the php requirement from the project.  The goal is to run the software locally with no web server required (JavaScript only).
- `future` contains issues about adding DHCPv6 configuration output support.

## Contributing
TBD

## License
Copyright &copy; 2023 by Internet Systems Consortium, Inc. (“ISC”)

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in 
all copies.

THE SOFTWARE IS PROVIDED “AS IS” AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY 
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

